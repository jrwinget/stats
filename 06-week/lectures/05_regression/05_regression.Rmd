---
title: "Regression"
author: "PSYC 304, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
#  Today's plan

+ Answer general questions before quiz 
  + 10-15 mins
+ Quiz
  + 35 mins
+ Lecture: Regression
+ Break
+ Continue regression lecture
+ Hand out homework

???

+ 

---
#  Review

+ What concepts have you had trouble with?

???

+ 

---
#  Quiz

+ End time:

???

+ 

---
#  Goals for today

+ When is regression used?
  + What is its purpose?
+ How does regression relate to correlation? 
+ Elements of a regression equation 
+ Using a regression equation to make predictions 
+ What $r$ tells us about usefulness in prediction
+ Unstandardized vs. standardized regression
+ Computing *standardized* regression coefficient
+ Computing *unstandardized* regression coefficient and intercept
+ $r^2$ = variance explained

???

+ 

---
#  Review: Correlation

.pull-left[<img src="assets/img/image1.png">]

.pull-right[
<br>
.center[
$r$ ranges from -1 to 1]

<br>
<br>
<br>
$$r = \frac{\sum z_x z_y}{n}$$
]

???

+ 

---
#  Correlation

.pull-left[
+ Question: How tall is Scott? 
+ Three clues: 
  + Men height $\mu = 64.8”$
  + He loves Dexter. 9.5 on a 10 point Dexter-appreciation scale, $z = 2.0$
      + Dexter appreciation and height $r = 0.0$
  + His dog is 36.56” tall, $z = 0.5$
      + Dog height and owner’s height $r = 0.65$
+ Which of these three pieces of information is useful? 
]

.pull-right[.center[
<img src="assets/img/image2.png" height=150><br>
<img src="assets/img/image3.png" height=150><br>
<img src="assets/img/image4.png" height=150>
]]

???

+ 

---
#  Correlation

+ Which of these three pieces of information is useful? 
  + **The mean**
      + Given no other info, the mean is the best guess you can give for $y$
  + **His dog’s height**
      + There is a notable correlation between dog height and their owner’s height ( $r = 0.65$)
  + Dexter info is NOT useful
      + There is no correlation between Dexter appreciation and height <br>( $r = 0.0$)

???

+ 

---
#  Correlation and prediction

+ The larger the $r$ (either in positive or negative direction!), the more useful it is for prediction ( $r$ of -0.9 is more useful than +0.3)

<br>
.pull-left[
.center[
$r = .95$

<img src="assets/img/image5.png" height=200>
]]

.pull-right[
.center[
$r = .50$

<img src="assets/img/image6.png" height=200>
]]

???

+ 

---
#  A tale of two scatterplots

.center[<img src="assets/img/image7.png">]

???

+ 

---
#  Correlation and regression

.pull-left[
+ Correlation can tell:
  + Whether $x$ and $y$ are related
  + How “tight” the data are 
  + The direction of the relationship
+ But not:
  + The slope of the line 
      + If a pterodactyl weighs 100 pounds, how tall is it expected to be? 
+ Correlation & regression use similar data, but answer different questions
  + Interval/ratio in both cases
]

.pull-right[.right[
<img src="assets/img/image8.png" height=150>
<img src="assets/img/image9.png" height=300>
]]

???

+ 

---
#  Regression and line of best fit

.pull-left[<br><img src="assets/img/image10.png">]

.pull-right[
<br>
<br>
<br>
<br>
<br>
+ Line of best fit: The straight line that best approximates all the dots
  + Also called a “regression line”
]

???

+ 

---
#  Regression and line of best fit

+ What do we mean by best line?
+ Much like the mean is the best point to describe a variable – because it minimizes the squared deviations around itself, we will try to define a line that minimizes the squared deviations between the point defined by the line (predicted Y or Y-hat) and the actual $y$ observed.
  + Called the “least squares criterion”

<br>
<br>
$$\sum_{i = 1}^{n} (y_i - \hat{y_i})^2$$ 
.center[Line of best fit minimizes this quantity]

???

+ 

---
#  Regression is for predictions

+ Correlation simply answers how much $x$ and $y$ “go together” (i.e., covary)
+ Regression helps you predict $y$ from $x$
  + Temperature -> ice cream demand
  + GRE score -> success in grad school
+ IF variables are correlated, regression can help us mathematically make educated guesses and predictions 

<br>
.center[
<img src="assets/img/image11.png" height=200>
<img src="assets/img/image12.png" height=200>
]

???

+ 

---
#  Regression is for predictions

+ Regression equations show you how $x$ and $y$ are related

<br>

$$y = bx + a$$

<br>

+ $y$ = dependent variable score
+ $a$ = y-intercept a.k.a. constant
+ $b$ = the slope of the line
+ $x$ = independent variable score

???

+ 

---
#  Regression equation

.pull-left[<br><img src="assets/img/image13.png">]

.pull-right[<br><br>
+ $b$ is the slope of the line
+ $a$ is the intercept, a.k.a. “constant”
  + Where the line intersects the y-axis
+ $x$ is the independent (predictor) variable
+ $y$ is the dependent (outcome) variable
]

???

+ 

---
#  Regression equation

$$y = bx + a$$

<br>

+ Just like with correlation, we have two variables, $x$ and $y$
+ We need to figure out which one is the independent (predictor) variable 
  + We call this $x$
+ The variable we are trying to predict is the dependent (outcome) variable
  + We call this $y$
+ Examples: 
  + GRE scores and grad school success ( $x$ = GRE, $y$ = Success)
  + Temperature and ice cream sales ( $x$ = Temperature, $y$ = Sales)
  + Final exam scores and minutes spent studying ( $x$ = Studying, $y$ = Scores)

???

+ 

---
#  Regression equation

.pull-left[<br>

$$y = bx + a$$

<br>

$b$ = The slope of the line <br>
The **"regression coefficient"**

<br>

<img src="assets/img/image14.png">
]

.pull-right[<img src="assets/img/image15.png">]

???

+ 

---
#  Regression equation

.pull-left[<br>

$$y = bx + a$$

$a$ = The y-intercept <br>
The constant


The point at which the line crosses the $y$ axis. Specifically, the value of $y$ when $x$ = 0

<br>

$$a = \overline{y} - b\overline{x}$$

]

.pull-right[<img src="assets/img/image17.png">]

???

+ 

---
#  Slope vs. intercept

<br>
.center[
<img src="assets/img/image18.png" height=400>
<img src="assets/img/image19.png" height=400>
]

???

+ 

---
#  Regression is for predictions

<br>

$$y = bx + a$$

<br>

+ $a$ and $b$ are what you can calculate using a real dataset
  + These days, computers are typically used
  + We’ll work through an example by hand
+ Once you have $a$ and $b$ figured out, you can plug in any new $x$ score and make a prediction for $y$
  + Predicted $y$ values are called $\hat{y}$ (y-hat)

???

+ 

---
#  ŷ vs. y

.pull-left[
+ A predicted dependent variable score is called $\hat{y}$
+ Pronounced “y-hat”
  + The book calls it y-cap, but that term is less common
+ A real $y$ score, that you actually observed, is just referred to as “y”
]

.pull-right[.center[<img src="assets/img/image20.png" height=450>]]

???

+ 

---
#  Regression equation

<br>
<br>
$$\hat{y_i} = bx + a$$

<br>
$$y_i = bx_{i} + a + e_i$$

<br>
.center[
Note: When discussing regression, we say we are <br>
“regressing $y$ onto $x$” in the above case.]

???

+ 

---
#  Regression equation

.pull-left[<img src="assets/img/image21.png">]

.pull-right[
$$y = bx + a$$

+ Size (ounces) = 0.96*age + 0.83
+ What is the predicted size for a baby dinosaur that is 4.50 days old?
  + $\hat{y}  = (0.96*4.50) + 0.83$
  + $\hat{y} = 4.32 + 0.83$
  + $\hat{y} = 5.15$

<br>
.right[<img src="assets/img/image22.png" height=200>]
]

???

+ Point out where this would be on the graph

---
#  Slope and intercept

+ Practice problem: 
  + $y = bx + a$
  + Size = 0.96*age + 0.83
  + What is the predicted size at birth (age = 0)?
  + What is the predicted size when age = 1?

<br>
<br>
<br>
.right[<img src="assets/img/image22.png" height=250>]

???

+ Predicted size at birth: 0.83 ounces
  + In other words, when x is 0, y is 0.83
  + This is a good example of how the a (**the y-intercept**) can be pretty intuitive (remember the definition of a: “value of Y when X is 0”)
+ Predicted size when age = 1: 1.70 ounces
  + Notice that for a 1 day increase in age, there is a 0.96 ounce (1.79 – 0.83) increase in size
  + This is a great example of what **“slope”** conceptually means! (A one day increase in age predicted a 0.96 ounce increase in size)

---
#  Simple, linear, bivariate regression

+ Our focus will be on “simple regression” 
  + Linear and just 2 variables
+ Not all regression is linear 
  + Relationships can be curved
+ Not always bivariate, either – often use 3, 4, 5, 6+ predictor variables – called “multiple regression”
  + E.g. in public health research: predict longevity ($y$) from smoking ( $x_1$), exercise ( $x_2$), amount of veggies eaten ( $x_3$), etc.
+ Bivariate
  + Linear relationship using one $x$ to predict one $y$

???

+ 

---
#  Regression line

+ Steps:
  1. Pick any two $x$ scores within your range
  1. Figure out the corresponding $\hat{y}$ score for each, using your equation
  1. Plot the two ( $x, \hat{y}$) pairs on a graph
  1. Draw a straight line between the points

<br>
+ Example: stress predicts wellbeing: 
  + $\hat{y} = 9.4 – 1.5x$
+ Stress scores range from 0 to 5

???

+ Stress picture?

---
#  Regression line

.pull-left[
+ $\hat{y} = 9.4 – 1.5x$; try $x = 0$ and $x = 4$


+ $x = 0$
+ $\hat{y} = 9.4 + –1.5(0)$
+ $\hat{y} = 9.4$
+ First $x, y$ pair = (0, 9.4)


+ $x$ = 4
+ $\hat{y} = 9.4 + –1.5(4)$
+ $\hat{y} = 9.4 – 6$
+ $\hat{y} = 3.4$
+ Second $x, y$ pair = (4, 3.4)
]

.pull-right[<br><img src="assets/img/image23.png">]

???

+ 

---
#  Regression line

.pull-left[
+ Yes, any two points! <br>(preferably within range)


+ $\hat{y} = 9.4 – 1.5x$
+ Let’s try 3
  + $\hat{y} = 9.4 – 1.5(3)$
  + $\hat{y} = 4.9$
]

.pull-right[<br><img src="assets/img/image24.png">]

???

+ Update picture?

---
#  Regression and error variance

.pull-left[<br><br><img src="assets/img/image25.png">]

.pull-right[
$$y = bx + a + e$$

+ Sometimes regression is represented that way, to acknowledge that any given $y$ score is determined by the intercept, the slope, the $x$ score, AND those error terms
+ Unless the correlation between $x$ and $y$ is perfect, the real data points can’t all be captured by a single line
+ “Error” doesn’t mean we messed up our regression equation!
]

???

+ 

---
#  Regression and error variance

+ One of the reasons we use regression is to better predict (reduce error) the value on some variable (like person’s height) by knowing some other variable


+ Without any knowledge, our best guess is the mean


+ With knowledge of a good predictor variable (dog’s height), we can reduce the variance in our predictions

???

+ 

---
class: inverse
background-image: url('assets/img/image26.png')
background-size: contain

???

+ 

---
#  Standard approach to error

+ Standard error of estimate:

$$s_{y*x}$$

+ Square root of the sum of the squared deviations of $y$ from $\hat{y}$
+ Tells us, on average, how far away our prediction should be overall
+ Just like a standard deviation tells us the average distance of points from the mean, the standard error of estimate tells us the average distance of points from the regression line

???

+ 

---
#  Standard error of the estimate

<br>
$$s_{y*x} = \sqrt{\frac{\sum_{i = 1}^{n} (y_i - \hat{y_i})^2}{n - 2}}$$

.center[Standard form]

<br>
<br>
$$s_{y*x} = \sqrt{[\frac{1}{n(n - 2)}][n\sum_{i=1}^{n} y^2 - (\sum_{i=1}^{n} y)^2 - \frac{[n\sum_{i=1}^{n} x_{i}y_{i} - (\sum_{i=1}^{n} x_i)(\sum_{i=1}^{n} y_i)]^2}{n\sum_{i=1}^{n} x^2 - (\sum_{i=1}^{n} x)^2}]}$$

.center[Calculation form – looks nasty but really just stuff we have done before.]

???

+ 

---
#  Calculate standard error

<br>
<br>

| $n = 5$ | $x \space$ (dog height) | $y \space$ (person height) | $x^2$ | $y^2$ | $xy$ |
|:---------:|:-------------:|:-----------:|:--------:|:----------:|:---------:|
| 1         | 32            | 60          | 1024     | 3600       | 1920      |
| 2         | 40            | 66          | 1600     | 4356       | 2640      |
| 3         | 39            | 74          | 1521     | 5476       | 2886      |
| 4         | 34            | 65          | 1156     | 4225       | 2210      |
| 5         | 27            | 59          | 729      | 3481       | 1593      |
| **Sum**   | **172**       | **324**     | **6030** | **21138**  | **11249** |

???

+ 

---
#  Example

$$s_{y*x} = \sqrt{[\frac{1}{n(n - 2)}][n\sum_{i=1}^{n} y^2 - (\sum_{i=1}^{n} y)^2 - \frac{[n\sum_{i=1}^{n} x_{i}y_{i} - (\sum_{i=1}^{n} x_i)(\sum_{i=1}^{n} y_i)]^2}{n\sum_{i=1}^{n} x^2 - (\sum_{i=1}^{n} x)^2}]}$$

<br>
$$s_{y*x} = \sqrt{(.0667)([5(21138) - (104976)] - \frac{[(5)(11249) - (172)(324)]^2}{5(6030) - (29584)})}$$

$$s_{y*x} = \sqrt{(.0667) ([105690 - 104976] - \frac{[56245 - 55728]^2}{566})}$$

$$s_{y*x} = \sqrt{(.0667)(714 - \frac{267289}{566})}$$

<br>
$$s_{y*x} = 4.02$$

???

+ 

---
#  Least squares regression

.pull-left[<br><br><img src="assets/img/image27.png">]

.pull-right[
+ Each of dotted line represents how far off the predicted line is from real data points 
  + Line’s error in prediction
+ Imagine we measured all those dotted lines (say 2, 4, -4, etc.), squared them (e.g. 4, 16, 16, etc.), and summed them up 
+ The idea is to draw a line that results in the lowest or “least” squared value of those errors
+ Essentially, we’re finding the line that best fits all of the points, and minimizes those  squared deviations
]

???

+ 

---
class: center, middle, inverse, title-slide

#  Worksheet:
##  *Front side*

???

+ 

---
#  Simple regression limitations

+ Although the regression line shows us the best line for describing the relationship between two variables, it does not tell us much else
  + It doesn’t tell us how strong the relationship is between the two variables
  + It doesn’t tell how good the line is in terms of prediction
  + It doesn’t allow us to really compare relations between variables

???

+ 

---
#  Examples

.pull-left[.center[
$\hat{y} = .913x + 33.39$ 

<br><br>
$\hat{y} = .724x - 12.52$

<br><br>
$\hat{y} = 2.98x - 11.01$

<br>
$\hat{y} = 4.3x - 137.1$

<br>
$\hat{y} = .047x + 58.94$
]]

.pull-right[
Predicting person height from dog height

<br>
Predicting dog height from person height

<br>
Predicting shoe size from height

<br>
Predicting weight from height

<br>
Predicting height from weight
]

???

+ 

---
#  How to make things more comparable

+ Basically, we do for our measure of relationships what we did for our relative standing measure
  + Standardize
+ If we transform both $x$ and $y$ into z-scores, they will both have means = 0 and standard deviations = 1
+ So, what happens to our regression equation when we standardize?

???

+ 

---
#  Unstandardized vs. standardized regression

<br>
<br>
.center[<img src="assets/img/image28.png">]

???

+ 

---
#  Unstandardized vs. standardized

+ Unstandardized regression deals in “raw score units”
  + The examples we’ve used so far
  + e.g., for every one day increase in age, how many ounces increase in size?
  + The slope is referred to with $b$
+ Standardized regression deals in “z-score units” 
  + For example, for every 1 SD increase in dog’s height, how much of an increase (in SD units) in owner’s height? 
+ Just like we can look at distribution either in terms of z-scores or raw scores, we can do the same with regression

???

+ 

---
#  B vs. beta

.pull-left[<br><img src="assets/img/image28.png">]

.pull-right[<br><br>
+ $b$ (or $B$) = unstandardized regression coefficient

<br>
<br>
+ $\beta$ (“beta”) = standardized regression coefficient 
]

???

+ 

---
#  Unstandardized regression

.pull-left[
+ Best used when you’re concerned with predicting “raw score” outcomes
+ Say you have a tomato plant, but not all of the tomatoes have grown in yet
+ Assume tomato plant height and tomato production are positively correlated
  + e.g., for each 1” of height, how many more ounces of tomato does a tomato plant produce?
  + Unstandardized regression coefficient, $b = 4.05$ tells you that for each in. of height, 4.05 more ounces of tomatoes are produced
]

.pull-right[.right[<img src="assets/img/image29.png" height=500>]]

???

+ 

---
#  Standardized regression

.pull-left[
+ Best used when concerned with z-scores (standardized units)
  + e.g., for each 1 SD increase in plant height, how many more SDs of tomatoes (oz.) are produced? 
  + $\beta = 0.5$ tells you a 1 SD increase in height relates to 0.5 SD more tomatoes produced
+ Really handy when you use multiple regression! 
  + Standardize all the predictions so you can compare them to each other
  + Amount of sun, amount of rain, brightness of leaf color, etc.
  + $\beta$ for sun $> \beta$ for leaf brightness? 
]

.pull-right[.right[<img src="assets/img/image29.png" height=500>]]

???

+ 

---
#  Standardized regression equation

<br>
$$z_y = \beta(z_x)$$

<br>
+ The z-score of $y$ = $\beta$ * (z-score of $x$)
+ Just like $b$ in the unstandardized regression equation ( $y = bx + a$), $\beta$ is a slope
  + $a$ is always zero (centered on the mean), so doesn’t even need to be mentioned	

???

+ 

---
#  Compute ß from r

<br>
<br>
<br>
<br>
.center[When we have simple, bivariate regression...]

<br>
<br>
$$\beta = r$$

???

+ 

---
#  Beta and r

+ For simple, bivariate regression, $\beta = r$


+ Since $r$ ranges from -1.0 to +1.0, $\beta$ will also range from -1.0 to +1.0


+ And similarly, $\beta = 0$ means the relationship is nada

???

+ 

---
#  Beta and r

+ If a correlation is positive and perfect, $\beta =$ ???
  + $\beta = 1.0$
  + $z_y = \beta(z_x)$
  + $z_y = 1.0(z_x)$

???

+ 

---
#  Standardized regression, graphed

+ Say stats class attendance and love of stats is perfectly correlated
+ If someone is 0.5 SD high on stats attendance, how high will their score be on stats love? 

.center[<img src="assets/img/image30.png">]

???

+ 

---
#  Converting to raw units

+ Given the info: 
  + Stats class attendance time has a mean of 1000 and SD of 100
  + Stats love has a mean of 6 and an SD of 2

<br>
.pull-left[
+ $x = (z_x * s) + \overline{x}$
+ $x = (0.5 * 100) + 1,000$
+ $x = 1,050$


+ $y = (z_y * s) + \overline{y}$
+ $y = (0.5 * 2) + 6$
+ $y = 7$
]

.pull-right[
+ Given these data:
  + $z_x, z_y$ of (0.5, 0.5) is equivalent to $x, y$ of (1050, 7)
]

???

+ 

---
#  Example: Standardized regression

+ Say commute time (min) is correlated with home satisfaction (scale of 1-10), $r = -0.3$
  + Jen’s commute is pretty short: $z = -1.5$ 


+ What is her predicted z-score on home satisfaction? 
  + $\beta = r = -0.3$
  + $z_y = \beta(z_x)$
  + $z_y = -0.3(-1.5)$
  + $z_y = 0.45$


+ In terms of the mean and standard deviation, what does that predicted $y$ mean?
+ As commute times go down, home satisfaction goes ?? 

???

+ 

---
#  Example: Standardized regression

.pull-left[
+ $z_x = 1.5, z_y = 0.45$
  + Commute example, $r = -.3$
+ $z_x = 0.5, z_y = 0.5$ 
  + Stats example, $r = 1$
+ This slope is less steep than the previous example
+ When $r$ is closer to 0, we make less extreme z-score predictions for $y$ (e.g. 1.5 -> 0.45)
+ Why? Remember, given no extra information, the mean is our best guess
+ With an $r$ of only 0.3, we won’t deviate so far from that mean
  + The stronger the $r$, the more confident we are that it’s going to make good predictions
]

.pull-right[<img src="assets/img/image31.png">]

???

+ 

---
#  Example: Standardized regression, noncorrelation

.pull-left[
+ Say percent of residents named Flanders (%) is not correlated with living situation satisfaction (1-10), $r = 0$
+ Kristen lives in a zip code with a ton of Flanders’ ( $z = 2.0$)
+ What is her predicted z-score on living satisfaction? 
  + $\beta = r = 0$
  + $z_y = \beta(z_x)$
  + $z_y = 0(2)$
  + $z_y = 0$
+ In other words, we’d simply use the mean to predict her satisfaction – Flanders’ aren’t helping
]

.pull-right[.center[<img src="assets/img/image32.png" height=400>]]

???

+ 

---
#  When r = 0

.pull-left[<br>
+ When $r = \beta = 0$


+ For ANY value of $x$, we’d simply guess 0 for $z_y$ (that is, the mean)


+ In other words, when $x$ and $y$ don’t correlate, we don’t use $x$ values to help predict $y$ 
  + Simply predict the mean
]

.pull-right[<img src="assets/img/image33.png">]

???

+ 

---
class: center, middle, inverse, title-slide

#  Unstandardized Regression

???

+ 

---
#  Unstandardized regression equation

<br>
<br>
<br>
<br>
$$y = bx + a$$

<br>
.center[
Let’s see how to compute the slope ( $b$) and y-intercept/constant ( $a$)]

???

+ 

---
#  Unstandardized regression coefficient (B)

<br>
<br>
<br>
<br>
$$b = r(\frac{s_y}{s_x})$$

<br>
.center[
Unstandardized regression coefficient = correlation coefficient times <br>
(standard deviation of $y$ divided by standard deviation of $x$)]

???

+ 

---
#  Example: Unstandardized regression slope

+ Mean commute time ( $x$) is 30 minutes, with a SD of 5 minutes
+ Mean home satisfaction ( $y$) is 6.5, with a SD of 2
+ Correlation between commute time and home satisfaction is -0.3
+ Compute $b$
  + $b = r(\frac{s_y}{s_x})$
  
  + $b = -0.3(2 / 5)$
  
  + $b = -0.12$

???

+ 

---
#  Computing y-intercept

<br>
<br>
<br>
<br>
$$a = \overline{y} - b\overline{x}$$

<br>
.center[
The y-intercept = Mean of $y$ minus (the unstandardized regression coefficient) multiplied by (the mean of $x$)]

???

+ 

---
#  Example: Unstandardized regression

+ Mean commute time ( $x$) is 30 minutes, with a SD of 5 minutes
+ Mean home satisfaction ( $y$) is 6.5, with a SD of 2
+ $b = -0.12$


+ Compute $a$
  + $a = \overline{y} - b\overline{x}$
  
  + $a = 6.5 – (-0.12)(30)$
  
  + $a = 6.5 – (-3.6)$
  
  + $a = 10.1$

???

+ 

---
#  Example: Unstandardized regression

<br>
<br>
<br>
<br>
$$\hat{y} = bx + a$$

<br>
<br>
$$\hat{y} = -0.12x + 10.1$$

???

+ 

---
#  Example: Unstandardized regression

+ Recall Jen’s commute is pretty short
  + $z = -1.5$


+ Recall mean commute time ( $x$) is 30 minutes, with a SD of 5 mins 


+ Jen’s raw x-score is 22.5 minutes 	
  + $x = z * s + \overline{x}$
  + $x = (-1.5)(5) + 30$


+ Calculate Jen’s home satisfaction using her commute time
  + $\hat{y} = 10.1 + -0.12(22.5)$ 
  + $\hat{y} = 10.1 – 2.7$
  + $\hat{y} = 7.4$ on the home satisfaction scale

???

+ 

---
#  Example: Unstandardized regression (equivalency of unstd. & std. reg.)

+ Jen’s predicted home satisfaction y-score is 7.4

+ What is the z-score for her predicted y-score? 

+ Recall mean home satisfaction ( $y$) is 6.5, with a SD of 2
  + $z = (\overline{x} – x) / s$
  
  + $z = (7.4 – 6.5) / 2$
  
  + $z_y = 0.45$

???

+ Seem familiar? See next slide

---
#  Example: Standardized regression equation RERUN (see slide 53)

+ Jen’s commute is pretty short: $z = -1.5$


+ What is her predicted z-score on home satisfaction? 
  + $\beta = r = -0.3$
  + $z_y = \beta(z_x)$
  + $z_y = -0.3(-1.5)$
  + $z_y = 0.45$


+ In terms of the mean and standard deviation, what does that predicted $y$ mean?
+ As commute times go down, home satisfaction goes ?? 

???

+ 

---
class: center, middle, inverse, title-slide

#  Worksheet
##  *Back side*

???

+ 

---
#  Other ways of computing regression

+ Our formulas have relied on knowing $r$, which you know you can calculate via z-scores
+ Helpful as we draw a connection between z-scores, correlation, and regression
+ However, there are other ways to calculate these stats, where you don’t need to already know $r$

<br>
<br>
<br>
$$b = \frac{n(\sum_{i-1}^{n} x_{i}y_{i}) - (\sum_{i-1}^{n} x_i)(\sum_{i-1}^{n} y_i)}{n(\sum_{i-1}^{n} x_{i}^{2}) - (\sum_{i-1}^{n} x_i)^2}$$

???

+ 

---
#  R-squared

+ Another important stat to know, whether you’re dealing with standardized or unstandardized regression
  + **R-squared** ( $r^2$) 
+ $r^2$ is the proportion of error reduction in predicting $y$ relative to guessing the mean
  + The percent of variance in $y$ explained by $x$ (true, but less accurate)
+ One way to calculate – you guessed it – simply square the correlation coefficient ( $r$)
  + Ranges from 0 to 1 
  + Always positive
+ The bigger the $r^2$, the better the line is at predicting real data points
  + $r^2  = 0$ means that 0% of the $y$ variable is “explained” by $x$

???

+ 

---
#  R-squared

.pull-left[.center[
$r = .95, r^2 =.9025$ <br>
(~90% variance explained) <br><br>
<img src="assets/img/image5.png">
The line tells you a lot about where any given point is; error isn’t a big part of the story
]]

.pull-right[.center[
$r = .50, r^2 = 0.25$ <br>
(25% of variance is explained) <br><br>
<img src="assets/img/image6.png"> <br><br>
The line tells you a moderate amount about where any given point is, but errors are pretty big in determining where the points are
]]

???

+ 

---
#  R-squared

+ Another way to calculate it...

$$r^2 = (SS_t – SS_e) / SS_t$$

<br>
+ As prediction errors get bigger, $r^2$ gets smaller


+ The bigger your errors in prediction, the smaller your $r^2$


+ .blue[
Errors in prediction are represented by $(y -  \hat{y})$ ]

???

+ 

---
#  What's ahead

+ We’ll have another RStudio day on Wednesday
+ We’ll pick up where we left off with the tidyverse, run some descriptive stats, create some graphs, and maaaybe get to correlations and regressions
+ If we get that far, will also discuss how to calculate
  + $r^2$
  + $b$ (unstandardized regression coefficient)
  + $a$ (y-intercept)
  + $\beta$ (standardized regression coefficient)

???

+ 

---
#  Data viz is important!

.pull-left[<br><br><img src="assets/img/image34.png">]

.pull-right[
| Property | Value |
|:---------:|:-------------:|
| Mean of $x$ in each case | 9 (exact) |
| Sample variance of $x$ in each case | 11 (exact) |
| Mean of $y$ in each case | 7.50 (to 2 decimal places) |
| Sample variance of $y$ in each case | 4.122 or 4.127 (to 3 decimal places) |
| Correlation between $x$ and $y$ in each case | 0.816 (to 3 decimal places) |
| Linear regression in each case | $y = 3.00 + 0.50x$ (to 2 decimal places) |
]

???

+ 
