# PSYC 304: Statistics

This repository contains lecture materials and syllabi for the Statistics course taught by Jeremy R. Winget, as part of the psychology major undergraduate curriculum at LUC.
