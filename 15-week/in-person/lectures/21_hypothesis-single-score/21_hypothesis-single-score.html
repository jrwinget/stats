<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <title>Hypothesis Testing with a Single Score</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 304, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Hypothesis Testing with a Single Score
### PSYC 304, Winget

---

#  Review: Probability to `\(z\)`

1. Draw the normal curve
1. Draw a line for the mean and label it 50% or .50
1. Mark where your given percentile is and label it (left of .50 if you’re talking about low scores `\(&lt;\)` the mean; right of .50 if you’re talking about &lt;br&gt; high scores `\(&gt;\)` the mean)
1. What is the difference between your given percentile and the mean? 
1. Look up that % in Appendix 2 Table A, and find the corresponding z-score for that mean to `\(Z\)` proportion
1. Make note of direction (positive or negative; above or below the mean), and that is your z-score

???

+ 

---
#  Review: Null and research hypotheses

+ **Research (alternative) hypothesis `\(H_1\)`:**  What the researcher proposes to be true. The means are different. The opposite of the null hypothesis. “There is an effect.” 
  + Directional (one-tailed): `\(Mean_1 &gt; Mean_2\)` **or** `\(Mean_1 &lt; Mean_2\)`
  + Non-directional (two-tailed): `\(Mean_1 \ne Mean_2\)`


+ **Null hypothesis `\(H_0\)`:** The default assumption; the “status-quo” assumption. “There is no effect.” The opposite of the research hypothesis.

???

+ 

---
#  Review: Probability to `\(z\)`

.pull-left[&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;img src="assets/img/image1.png"&gt;]

.pull-right[&lt;br&gt;&lt;br&gt;
+ What is the z-score that corresponds to lower than 99% of the population? 
+ We know it’s negative (because it’s below the mean)
  + In fact, .49 below the mean 
  + `\(.99 \space – .50 = .49\)`
+ Look up the corresponding &lt;br&gt; z-score for a mean-to-z proportion of .49
]

???

+ Z for .49 is 2.33
+ So, this z-score is -2.33! 
+ Which means what?
  + 2.33 SD below the mean

---
#  Review: Stating the hypothesis

+ I hypothesize that people who have dogs will have a different level of amusement when watching pet videos on YouTube, compared to the general population’s amusement about those videos. But I’m not sure whether they will be more or less amused. I talk to my friend Rachel, who has a bulldog, and rate her amusement toward a certain YouTube pet video. I compare her amusement to the known population mean amusement. I’m not too worried about making “false alarms” in this case, so I pick a confidence level of .90.

--

+ `\(H_1\)`: `\(\mu_1 \ne \mu_2\)`

--

+ `\(H_0\)`: `\(\mu_1 = \mu_2\)`

--

+ `\(C = .90\)`

--

+ `\(N = 1\)`

--

+ `\(\alpha = .10\)`

--

+ Hypothesis is .blue[**non-directional**]

???

+ H1 : µ1 ≠ µ2 			
+ H0 : µ1 = µ2
+ C = .90
+ N = 1
+ α = .10
+ Hypothesis is non-directional

---
#  Hypothesis testing: Food adventurousness

Some people are very adventurous with food: they are willing to taste just about anything. Others shy away from anything but the meat and potatoes they grew up with. But most people are somewhere between these two extremes. In fact, willingness to try new foods is normally distributed. Say that on our “food-adventurousness” scale, there is a **population mean of 20 and a standard deviation of 5.**

&lt;br&gt;
.center[
&lt;img src="assets/img/image2.png" height=200&gt;
&lt;img src="assets/img/image3.png" height=200&gt;
&lt;img src="assets/img/image4.png" height=200&gt;
]

???

+ 

---
#  Hypothesis testing: One-tailed test

+ Suppose we think that open-minded people are more willing to try new foods


+ We talk to our friend Joe who we know is very open-minded, and find out that the scores a 34 on our measure of food-adventurousness. Is Joe’s score extremely high enough to be considered evidence in favor of our hypothesis?

&lt;br&gt;
&lt;br&gt;
.center[
&lt;img src="assets/img/image2.png" height=200&gt;
&lt;img src="assets/img/image5.png" height=200&gt;
&lt;img src="assets/img/image4.png" height=200&gt;
]

???

+ 

---
#  Hypothesis testing steps

1. Articulate the research and null hypothesis
1. Describe the comparison distribution
1. Determine critical value and alpha region
1. Determine the score that represents your sample and compare with the critical value
1. Comment on the .blue[**null**] hypothesis

???

+ 

---
##  Step 1: Articulate research and null hypothesis

+ `\(H_1\)`: Open-minded people will be more willing to try new foods than people in general
  + `\(H_1: \mu_1 &gt; \mu_2\)` 


+ `\(H_0\)`: Open-minded people will be equally or less willing to try new foods than people in general
  + `\(H_0: \mu_1 ≤ \mu_2\)`
  + .red[**Remember:**] `\(\mu_1\)` means the mean of the special population we expect to differ, `\(\mu_2\)` is the mean of the general population we’re comparing to


+ By the way, is our research hypothesis a directional (one-tailed) or non-directional (two-tailed) hypothesis? 

???

+ Answer: Directional

---
##  Step 2: Describe comparison distribution

+ State what the population is, its mean, and its standard deviation 
  + The distribution of scores from the general population
  + Population mean = 20
  + Population standard deviation = 5

.center[
&lt;img src="assets/img/image3.png" height=300&gt;&lt;br&gt;
`\(\mu = 20\)`&lt;br&gt;
`\(s = 5\)`
]

???

+ 

---
##  Step 3: Determine critical value and alpha region

+ Let’s be conventional and set our alpha level to be `\(.05\)` `\((5\%)\)` – the typical alpha level set in behavioral research
+ Convert our `\(\alpha\)` into a critical value (in this case, a certain z-score!)
+ We’re talking about a given score being *extremely high or large*, so we draw a line on the right side of the mean  

&lt;br&gt;
.center[&lt;img src="assets/img/image6.png" width=95%&gt;]

???

+ 

---
##  Step 3: Determine critical value and alpha region

+ Convert our alpha region into a z-score: 
  + `\(\alpha = .05\)`; higher than `\(95\%\)` of all other scores
+ z-score that corresponds to a mean-to-z proportion of `\(45.00\%\)` `\((95\% - 50\%)\)`
+ In the table, we see that is...
  + Exactly between `\(1.64\)` and `\(1.65\)`; convention is to go with `\(1.64\)`
+ `\(z = 1.64\)` (we’re concerned with high scores, so above the mean)

&lt;br&gt;
.center[&lt;img src="assets/img/image7.png" width=50%&gt;]

???

+ 

---
##  Step 4: Determine score that best represents sample and compare to critical value

+ We have a raw “food adventurousness” score for Joe: `\(34\)`
+ Can we compare `\(34\)` to the critical value.... `\((1.64)\)`? 
  + Not yet...
+ First, convert Joe’s raw score to a z-score

&lt;br&gt;

`$$z = \frac{(X – \overline{X})}{s}$$`

&lt;br&gt;

+ From earlier slide: “there is a population mean of 20 and a standard deviation of 5”
  + Joe’s `\(z = (34 \space – 20) / 5\)`
  + Joe’s `\(z = 14 / 5\)`
  + Joe’s `\(z = 2.8\)`

???

+ 

---
##  Step 4: Determine score that best represents sample and compare to critical value

+ Joe’s `\(z = 2.8\)`
+ Critical value is `\(z\)` of `\(1.64\)` or higher 
+ `\(2.8 &gt; 1.64\)`
+ Joe’s z-score is **higher than our critical value**

.center[&lt;img src="assets/img/image8.png" width=70%&gt;]

???

+ 

---
##  Step 5: Comment on null hypothesis

+ `\(H_0\)` = "Open-minded people will be equally or less willing to try new foods than people in general”
  + **We .red[reject] this null hypothesis**


+ Some other ways of thinking about that
  + Joe’s score (admittedly a sample size of one) is so extreme that we reject the null hypothesis
  + We found some support for `\(H_1\)`
      + Open-minded people will be more willing to try new foods than people in general
  + There is evidence for an “effect” of open-mindedness on food-adventurousness
  + But, we accept a small chance that this is due to a random fluctuation
      + `\(\alpha = .05\)`

???

+ 

---
#  Hypothesis testing: Two-tailed test

Suppose you think that people who have recently eaten a lot of plain foods in the past few days might differ from people in general in their willingness to try new foods – but you are not sure whether they are more or less willing. Your friend Rick who has been eating nothing but chicken nuggets (and szechuan sauce) and fries scores a 23 on your measure of willingness to try new foods. Conduct a hypothesis test with an alpha of .05 to examine your idea

&lt;br&gt;
&lt;br&gt;
.center[
&lt;img src="assets/img/image9.png" height=200&gt;
&lt;img src="assets/img/image10.png" height=200&gt;
]

???

+ 

---
##  Step 1: Articulate research and null hypothesis

+ `\(H_1: \mu_1 \ne \mu_2\)`
  + People who have recently eaten only bland foods will DIFFER in their mean score of food adventuresome-ness from the general population


+ `\(H_0: \mu_1 = \mu_2\)`
  + People who have recently eaten only bland foods will be EQUAL in their mean score of food adventuresome-ness from the general population


+ Non-Directional hypothesis, so you know this is a “two-tailed test”
  + No “greater than or less than” language


+ .red[**As a reminder, the null hypothesis always includes the possibility that the population means are equal**]

???

+ 

---
##  Step 2: Describe comparison distribution

+ It is the distribution of general population scores
+ The mean is 20
+ The standard deviation is 5

&lt;br&gt;
.center[
&lt;img src="assets/img/image11.png" width=80%&gt;&lt;br&gt;
`\(\mu = 20\)` &lt;br&gt;
`\(s = 5\)`
]

???

+ 

---
##  Step 3: Determine critical value and alpha region

.pull-left[&lt;br&gt;
+ `\(\alpha = .05\)` (or `\(5\%\)`)


+ But this time, our alpha region has to be “split up” between the two tails!


+ If either food adventurousness is extremely high or extremely low, we would reject the null hypothesis


+ Take our alpha and split it into two, `\(2.5\%\)` `\((.025)\)` on the high end, and `\(2.5\%\)` `\((.025)\)` on the low end
]

.pull-right[&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;img src="assets/img/image12.png" width=100%&gt;]

???

+ 

---
##  Step 3: Determine critical value and alpha region

.pull-left[&lt;br&gt;
+ Now we need two “cutoff scores”, but since it’s symmetrical, let’s start with just the positive side


+ What is the maean-to-z proportion we need to find now? 
  + `\(50\% - 2.5\% = 47.5\%\)`


+ Z-score that corresponds to `\(47.5\% = 1.96\)`


+ So the z-score that corresponds to `\(47.5\%\)` **BELOW** the mean would be.... `\(-1.96\)`
]

.pull-right[&lt;br&gt;&lt;br&gt;&lt;br&gt;&lt;img src="assets/img/image13.png" width=100%&gt;]

???

+ 

---
##  Step 4: Determine score that best represents sample and compare to critical value

.pull-left[
+ Rick’s score is 23; 
  + Mean = 20; SD = 5


`$$z = \frac{(X - \overline{X})}{s}$$`


+ `\(z = (23 \space – 20) / 5\)`
+ `\(z = 0.6\)`
+ Is 0.60 larger than 1.96? .red[**NOPE!**]
+ Is 0.60 lower than -1.96? .red[**NOPE!**]
+ This score *does not* fall into the alpha region
+ You could say it falls in the “non-rejection” region
  + Between the two cutoff scores
]

.pull-right[&lt;br&gt;&lt;br&gt;&lt;img src="assets/img/image14.png" width=110%&gt;]

???

+ 

---
##  Step 5: Comment on null hypothesis

+ `\(H_0: \mu_1 = \mu_2\)` 
  + People who have recently eaten only bland foods will be **EQUAL** in their mean score of food adventurousness to the general population’s mean


+ We did not obtain a value beyond our critical z-scores
  + Therefore we .red[**fail to reject**] the null hypothesis


+ Notice that we don’t say "accept the null hypothesis"
  + Always the possibility of a missed effect (Type II error) due to alpha level


+ We also don't say “disproved the research hypothesis”
  + In research, we are continuously gathering evidence for or against null hypotheses – this is not definitive!

???

+ 

---
#  Video interlude

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
.center[&lt;iframe width="560" height="315" src="https://www.youtube.com/embed/HoqzIR8xj4s?start=644" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen&gt;&lt;/iframe&gt;]

???

+ Start around 10:44

---
#  Video review

+ Notice: what happens when you increase alpha?
+ When you increase alpha, you have a greater chance of...?
  + Rejecting the null hypothesis 
  + Failing to reject the null hypothesis 


+ Therefore the Type I error rate goes...?
  + Up
  + Down


+ And the Type II error rate goes...? 
  + Up
  + Down

???

+ Reject the null
+ Up
+ Down

---
#  Video review

+ When we believe we have “an effect,” one way to think of that is that there are really TWO distributions here:
  + e.g., the general/comparison population (e.g., all people) `\((\mu_2)\)`, and our population of interest (e.g., open-minded people): `\(\mu_1\)`
+ Since the score we obtained seems rather weird or unusual for the overall distribution, we think its actually coming from a unique population, something that looks like the picture below 
+ That’s why you see hypotheses written like `\(\mu_1 &gt; \mu_2\)`

.center[&lt;img src="assets/img/image15.png"&gt;]

???

+
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
