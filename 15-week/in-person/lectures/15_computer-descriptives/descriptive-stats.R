library(skimr)
library(psych)
library(tidyverse)

# import data
class_survey <- read_csv("./descriptive-stats/2020_class-survey.csv") # readr package

# explore the data frame
View(class_survey) # base r
glimpse(class_survey) # dplyr package, part of the tidyverse
class_survey$music # select variable with $ in base r

# more detailed examination of the data
summary(class_survey) # base r
skim(class_survey) # skimr package
describe(class_survey) # psych package, note the errors caused by character variables
describe(class_survey$screen_time)

# create a frequency table for movies
class_survey %>%
  count( # dplyr package, part of the tidyverse
    movies,
    name = "freq"
  ) %>% 
  mutate( # dplyr package, part of the tidyverse
    cum_freq = cumsum(freq),
    rel_freq = prop.table(freq),
    cum_rel_freq = cumsum(rel_freq)
  )

# convert coffee raw scores into coffee z-scores
class_survey %>%
  select(coffee) %>% # dplyr package, part of the tidyverse
  mutate( # dplyr package, part of the tidyverse
    avg_coffee = mean(coffee),
    coffee_zscore = (coffee - mean(coffee)) / sd(coffee),
  ) %>% 
  arrange(desc(coffee_zscore)) # dplyr package, part of the tidyverse

############################################################################################################################
# LEARNING CHECK                                                                                                           #
# What does any ONE row in the data set refer to?                                                                          #
# What are some examples of categorical variables in this data set? What makes them different than quantitative variables? #
# What does int, dbl, and chr mean in the output from glimpse(class_survey)?                                               #
############################################################################################################################

# histogram for the concerts variable using ggplot2 package, part of the tidyverse
ggplot(
  data = class_survey,
  mapping = aes(x = concerts)
) +
  geom_histogram()

ggplot(
  data = class_survey,
  mapping = aes(x = concerts)
) +
  geom_histogram(
    bins = 10, # number of bins
    color = "white" # helps distinguish columns
  )

ggplot(
  data = class_survey,
  mapping = aes(x = concerts)
) +
  geom_histogram(
    bins = 10,
    color = "white",
    fill = "steelblue" # type colors() in the console to see all 657 available colors
  )

ggplot(
  data = class_survey,
  mapping = aes(x = concerts)
) +
  geom_histogram(
    binwidth = 2, # number of units per bin
    color = "white",
    fill = "steelblue"
  )

# frequency polygon for the Concerts variable using ggplot2 package, part of the tidyverse
ggplot(
  data = class_survey,
  mapping = aes(x = concerts)
) +
  geom_freqpoly(
    color = "steelblue",
    binwidth = 2
  ) +
  labs( # lets you edit the labels in the graph
    x = "Concerts",
    y = "Frequency",
    title = "Frequency polygon for number of concerts attended in the past year"
  )

###########################################################################################################################
# LEARNING CHECK                                                                                                          #
# What does changing the number of bins from 30 to 10 tell us about the distribution of concerts?                        #
# Would you classify the distribution of concerts as symmetric or skewed?                                                 #
# What would you guess is the "center" value in this distribution? Why?                                                   #
# Is this data spread out greatly from the center or is it close? Why?                                                    #
###########################################################################################################################

# barplot (for distributions) for the vacation variable using ggplot2 package, part of the tidyverse
# there are 2 ways of creating barplots, and they each depend on how the counts are represented in the data

# for example, fruits data frame will not be pre-tabulated
fruits <- tibble(
  fruit = c("apple", "apple", "apple", "orange", "orange")
)

glimpse(fruits)

# and fruits_counted will be pre-tabulated
fruits_counted <- tibble(
  fruit = c("apple", "orange"),
  freq = c(3, 2)
)

glimpse(fruits_counted)

# for data that are not pre-tabulated, use geom_bar()
ggplot(
  data = fruits,
  mapping = aes(x = fruit)
) +
  geom_bar()

# for data that are pre-tabulated, use geom_col()
ggplot(
  data = fruits_counted,
  mapping = aes(x = fruit, y = freq)
) +
  geom_col()

# now, consider the Vacation variable in the class class_survey (is it pre-tabulated or not?)
glimpse(class_survey)

ggplot(
  data = class_survey,
  mapping = aes(x = vacation)
) +
  geom_bar()

vacation_table <- class_survey %>% # create table that displays count for each vacation location
  group_by(vacation) %>%
  summarize(freq = n())

vacation_table # this creates a pre-tabulated count of the Vacation variable

ggplot(
  data = vacation_table,
  mapping = aes(x = vacation, y = freq)
) +
  geom_col() # now we can use geom_col on this new vacation_table object

###########################################################################################################################
# LEARNING CHECK                                                                                                          #
# Why are histograms inappropriate for visualizing categorical variables?                                                 #
# What is the difference between histograms and barplots?                                                                 #
# How many students in class would prefer to visit San Francisco?                                                         #
###########################################################################################################################

# sometimes may want to compare more than one categorical variable in a barplot (do so with caution!)
ggplot(
  data = class_survey,
  mapping = aes(x = vacation, fill = gender)
) +
  geom_bar(position = "dodge")

# if you do not want the gender variable on the same graph, you can facet them
ggplot(
  data = class_survey,
  mapping = aes(x = vacation, fill = gender)
) +
  geom_bar() +
  facet_grid(gender ~ .) # horizontal

ggplot(
  data = class_survey,
  mapping = aes(x = vacation, fill = gender)
) +
  geom_bar() +
  facet_grid(. ~ gender) # vertical
