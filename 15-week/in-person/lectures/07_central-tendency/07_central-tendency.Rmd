---
title: "Measures of Central Tendency"
author: "PSYC 304, Winget"
output:
  xaringan::moon_reader:
    css: ["default", "default-fonts", "my-style.css", "metropolis-fonts"]
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---
#  Single number descriptions

+ Measures of central tendency describe variables by their most common, central, or typical response


+ Most common form: Mean


+ Other forms: Median and mode

<br>
<br>
<br>
.right[<img src="assets/img/image1.jpeg" width=350>]

???

+ So this week, we're talking about how best to describe the data we've collected
+ Last class, we discussed how frequency distributions are one way of describing the values we've collected for a single variable...and how we can represent variables and frequency distributions graphically
+ But we can also summarize data numerically
+ In fact, probably the most basic kind of statistics are the ones we compute to describe scores for a single variable
+ 
+ In describing scores for a single variable, we'll often want to know what kinds of scores are typical...or common
+ This is a concept known as the central tendency of a distribution...and it's our focus of our discussion today
+ The most common way to describe a sample of data is by using measures of central tendency
+ The most common measure of central tendency is something you're all already familiar with...the average
+ In statistics, we refer to the average as the mean
+ There are a couple of other measures of central tendency as well though
  + They are the median and the mode
+ We'll break down all of these today...probably in more detail than you want...but for good reason
+ Measures of central tendency are the fundamental building blocks for not only descriptive statistics...but inferential statistics as well
+ So it's very important that you understand everything we talk about today...because we'll be using these concepts for the remainder of the semester

---
#  Central tendencies: Mean

+ To calculate mean, simply sum all of the responses and divide by the number of responses $(N)$

<br>
$$\overline{X} = \frac{\sum_{i = 1}^{N} X_i}{N}$$

<br>
.right[<img src="assets/img/image2.jpg" width=300>]

???

+ Let's start with the mean...or the average
+ To calculate the mean, we simply add up all of the responses we've collected...and divide by the total number of responses we have
  + Recall that N stands for sample size
  + We'll come across this symbol a lot...so any time you see N...think sample size...or the total number of values collected
  + As we go along, I will use N as shorthand, so I don't want anyone to get confused if I just say N instead of sample size or total number of responses
+ Here's, the formula for the mean
+ Explain formula
  + Remind them that $\overline{X}$ is read as "x bar" and it stands for the mean

---
#  Central tendencies: Sample mean

+ The sample mean $(\bar{X})$ equals the sum $(\sum)$ of all the values $(X_N)$ divided by the number of values $(N)$

<br>
<br>
$$\overline{X} = \frac{(X_1 + X_2 + ... + X_N)}{N}$$


<br>
<br>
$$\overline{X} = \frac{\sum X}{N}$$

???

+ Now, recall from the first week of class that I mentioned we use different symbols to refer to the sample and the population
+ In this case, we're interested in the sample mean
+ So to calculate x-bar, we simply sum up all the values in the sample and then divide by the sample size
+ Walk through formula

---
#  Central tendencies: Population mean

+ Same steps, slightly different formula
  + $\mu$ rather than $\overline{X}$

<br>
$$\mu = \frac{\sum X}{N}$$

<br>
.right[<img src="assets/img/image3.png" width=350>]

???

+ When it comes to the population, the procedures are essentially the same, but we use a slightly different formula
+ Here, instead of using x-bar, we use the population mean symbol
+ The population mean is represented by the Greek letter mu...which is this funny looking U here
+ So, this is the formula we use to calculate the population mean
  + Explain formula

---
#  Example: Mean

.pull-left[
```{r tbl1, echo = FALSE}
tbl1 <- tibble::tribble(
~`Number of texts per day`,
128, 61, 61, 99, 27, 188, 43, 300, 33, 61, 145, 284, 128, 114
)

kableExtra::kable_styling(knitr::kable(tbl1, format = "html", escape = FALSE, align = c("cc")), font_size = 18)
```
]

.pull-right[
$$\overline{X} = \frac{(X_1 + X_2 + ... + X_N)}{N}$$
<br>
$$\overline{X} = \frac{128 + 61 + 61 + 99 + 27 + 188 + 43\atop + 300 + 33 + 61 + 145 + 284 + 128 + 114}{14}$$
<br>
$$\overline{X} = \frac{1672}{14}$$
<br>
$$\overline{X} = 119.43$$
]

???

+ 

---
#  Example: Mean

.pull-left[
<br>
<br>
```{r tbl2, echo = FALSE}
tbl2 <- tibble::tribble(
~`Minutes watching Netflix`,
70, 840, 45, 60, 65, 0, 0
)

kableExtra::kable_styling(knitr::kable(tbl2, format = "html", escape = FALSE, align = c("cc")), font_size = 22)
```
]

.pull-right[
$$\overline{X} = \frac{(X_1 + X_2 + ... + X_N)}{N}$$
<br>
$$\overline{X} = \frac{70 + 840 + 45 + 60 + 65 + 0 + 0}{7}$$
<br>
$$\overline{X} = \frac{1080}{7}$$
<br>
$$\overline{X} = 154.9$$
]

???

+ 

---
#  Class data on screen time

.pull-left[
<br>
```{r tbl3, echo = FALSE, message=FALSE}
library(tidyverse)

screen_time <- read_csv(here::here("15-week/survey/2020_class-survey.csv")) %>% 
  select(screen_time)

freq_table <- function(x){
  my_table <- data.frame(table(x))
  my_table
}

freq_screen <- freq_table(screen_time) %>% 
  transmute(
    Response = x,
    Frequency = Freq
  )

kableExtra::kable_styling(knitr::kable(freq_screen, format = "html", escape = FALSE, align = c("cc")), font_size = 20)
```
]

.pull-right[
$$\overline{X} = \frac{(X_1 + X_2 + ... + X_N)}{N}$$
<br>
$$\overline{X} = \frac{2+2+2+2+2+3+3+3+3 \\ +3.5+3.5+3.5+4+4+4+4 \\ +4+4+4+5+5+5+5+5 \\ +5+5+5+5+5.5+5.5+5.5 \\ +5.75+6+6+7}{35}$$
<br>
$$\overline{X} = \frac{146.75}{35}$$
$$\overline{X} = 4.19$$
]

???

+ 

---
#  Central tendencies: Mean

<br>

| Class <img width=100/> | $N$ <img width=200/> | Sum <img width=200/> | $\overline{X}$ <img width=200/> |
|:---------:|:-----:|:---------:|:-------------:|
| Ours      | 35    | 146.75    | 4.19          |
| 1         | 33    | 166       | 5.03          |
| 2         | 30    | 208.5     | 6.95          |
| 3         | 15    | 112       | 7.47          |
| 4         | 17    | 156       | 9.18          |
| 5         | 28    | 194       | 6.93          |
| 6         | 47    | 302       | 6.43          |
| 7         | 34    | 95        | 2.79          |
| **Total** |**239**|**1380.25**|**5.78**       |

???

+ Means are useful for comparing different samples
+ Point out that in order to calculate the grand mean...they must use the totals from the other columns
  + Cannot take a mean of means

---
#  Central tendencies: Mean

+ Sum of deviations from mean will always equal 0

$$\sum_{i = 1}^N (X_i - \overline{X}) = 0$$

+ Proof:

$$\sum_{i - 1}^N (X_i - \overline{X}) = \sum_{i = 1}^N X_i - \sum_{i = 1}^N \overline{X}$$

$$\sum_{i = 1}^N (X_i - \overline{X}) = \sum_{i = 1}^N X_i - N \overline{X}$$

$$\sum_{i = 1}^N (X_i - \overline{X}) = \sum_{i = 1}^N X_i - N(\frac{\sum_{i - 1}^N X_i}{N})$$

$$\sum_{i = 1}^N (X_i - \overline{X}) = \sum_{i = 1}^N X_i - \sum_{i = 1}^N X_i = 0$$

???

+ To prove the sum of the deviations from the mean will always equal zero:....(steps below correspond to steps in proof)
1. The sum of the differences between 2 quantities equals the difference between their sums
1. The sum of the mean added to itself N times, is N times the mean
1. In the next step, we can substitute the equation for the mean...shown in parentheses
1. Next, we can cancel out the Ns using basic arithmetic...which leaves us with subtracting the sum of X from the sum of X...getting 0

---
#  Example: Mean 


<br>
$$\overline{X} = 3$$

<br>

| <img width=100/> $X_i$ <img width=100/> | <img width=100/> $(X_i - \overline{X})$ <img width=100/> |
|:---------:|:-----:|
| 1         | -2    | 
| 2         | -1    |
| 3         | 0     |
| 4         | 1     | 
| 5         | 2     |
| **Sum =** |**0**  |

???

+ Let's put this in more concrete terms...

---
#  Central tendencies: Mean

+ Mean minimizes sum of squared deviations
  + $\sum_{i = 1}^{N} (X_i - \bar{X})^2 <$ any other possible value


<br>
+ Short proof:

$$\sum_{i = 1}^{N} (X_i - (\overline{X} + c))^2, c \ne 0$$

$$\sum_{i = 1}^{N} ((X_i - \overline{X}) - c)^2 = \sum_{i - 1}^{N} [(X_i - \overline{X})^2 - 2c(X_i - \overline{X}) + c^2]$$

$$\sum_{i = 1}^{N} ((X_i - \overline{X}) - c)^2 = \sum_{i = 1}^{N} (X_i - \overline{X})^2 + Nc^2 > \sum_{i = 1}^{N} (X_i - \overline{X})^2$$


???

+ Another property of the mean concerns the squared deviations of scores about the mean
+ The sum of the squared deviations of scores about the mean is less than the sum of the squared deviations of the same scores about any other value
+ Basically, this is telling us why the mean is an appropriate measure of central tendency
  + The mean is closer to the individual scores over the entire group of scores than is any other single value
+ And here's the mathematical proof to show that
+ I'm not going to go into too much detail about this now...but see page 58 for more details on this proof

---
#  Example: Mean 

<br>
$$\overline{X} = 3$$

<br>

| <img width=100/> $X_i$ <img width=100/> | <img width=100/> $(X_i - \overline{X})^2$ <img width=100/> |
|:---------:|:-----:|
| 1         | 4    | 
| 2         | 1    |
| 3         | 0    |
| 4         | 1    | 
| 5         | 4    |
| **Sum =** |**10**|

???

+ Instead, this concept is easier to show with a concrete example
+ Explain table and values
+ The mean is closer to the individual scores over the entire group of scores than is any other single value

---
#  Central tendencies: Median

.pull-left[
+ Point that splits sample in half
  + “Middle” value 


+ If odd number of cases, it will be the $(N + 1) / 2$ case 
  + $[2, 3, 5, 6, 7, 9, 11]$ 
  + Median is fourth from bottom (6)


+ If even number of cases, it will be exactly halfway between the $N / 2$ case and the $N / 2 + 1$ case 
  + $[2, 3, 5, 6, 7, 9, 11, 12]$
  + Median is 6.5
  + Halfway between 6 and 7
]

.pull-right[
<br>
```{r tbl4, echo = FALSE}
tbl2 %>% 
  dplyr::arrange(`Minutes watching Netflix`) %>% 
  dplyr::transmute(
    `Minutes watching Netflix` = kableExtra::cell_spec(`Minutes watching Netflix`, color = ifelse(`Minutes watching Netflix` == 60, "red", "black"))
  ) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 22)
```
]

???

+ 

---
#  Examples: Median

.pull-left[
```{r tbl5, echo = FALSE}
tbl1 %>% 
  dplyr::arrange(`Number of texts per day`) %>% 
  dplyr::transmute(
    `Number of texts per day` = kableExtra::cell_spec(`Number of texts per day`, color = ifelse(`Number of texts per day` == 99 | `Number of texts per day` == 114, "red", "black"))
  ) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 18)
```
]

.pull-right[
+ And, the median is...?

<br>
<br>
<br>
<br>
<img src="assets/img/image4.png">
]

???

+ 106.5

---
#  Example: Mode

.pull-left[
+ Most frequent (common) response


+ Works for **all** measurement scales
  + Medians/means cannot be used with nominal data
  + Mode does not pay attention to the numbers themselves...only response at a particular point
]

.pull-right[
```{r tbl6, echo=FALSE}
tbl1 %>% 
  dplyr::arrange(`Number of texts per day`) %>% 
  dplyr::transmute(
    `Number of texts per day` = kableExtra::cell_spec(`Number of texts per day`, color = ifelse(`Number of texts per day` == 61, "red", "black"))
  ) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 18)
```
]

???

+ 

---
#  Example: Mode

.pull-left[
<br>
```{r tbl7, echo = FALSE}
tbl2 %>% 
  dplyr::arrange(`Minutes watching Netflix`) %>% 
  dplyr::transmute(
    `Minutes watching Netflix` = kableExtra::cell_spec(`Minutes watching Netflix`, color = ifelse(`Minutes watching Netflix` == 0, "red", "black"))
  ) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 22)
```
]

.pull-right[<br>
+ And, the mode is...?

<br>
<br>
<img src="assets/img/image5.png" width=100%>
]

???

+ 0

---
#  Central tendencies: Mode

.pull-left[
+ What if there’s more than one mode?
  + Then, more than one mode
  + Here, mode = 1, 3 


+ What if data are $[1, 2, 5, 7, 8]$? 
  + Mode = 1, 2, 5, 7, 8
  + Not very useful!
]

.pull-right[
```{r tbl8, echo = FALSE}
tbl4 <- tibble::tribble(
~`Pets in household`,
0, 1, 1, 1, 2, 3, 3, 3, 4, 5
)

tbl4 %>% 
  dplyr::arrange(`Pets in household`) %>% 
  dplyr::transmute(
    `Pets in household` = kableExtra::cell_spec(`Pets in household`, color = ifelse(`Pets in household` == 1 | `Pets in household` == 3, "red", "black"))
  ) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 22)
```
]

???

+ 

---
##  Central tendencies: Mean vs. median vs. mode

.pull-left[
```{r tbl9, echo=FALSE}
tbl1 %>% 
  dplyr::arrange(`Number of texts per day`) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 18)
```
]

.pull-right[
<br>
<br>
<br>
<br>
+ Mean
  + Average = 119.43


+ Median
  + Middle point = 106.5


+ Mode
  + Most common = 61
]

???

+ 

---
##  Central tendencies: Mean vs. median vs. mode

.pull-left[
<br>
<br>
```{r tbl10, echo=FALSE}
tbl2 %>% 
  dplyr::arrange(`Minutes watching Netflix`) %>% 
  knitr::kable(format = "html", escape = FALSE, align = c("c")) %>% 
  kableExtra::kable_styling(font_size = 22)
```
]

.pull-right[
<br>
<br>
<br>
<br>
+ Mean
  + Average = 154.29


+ Median
  + Middle point = 60


+ Mode
  + Most common = 0
]

???

+ Measures of central tendency are all over the place...what's going on here?

---
class: inverse
background-image: url('assets/img/image8.png')
background-size: contain

???

+ Mean is especially sensitive to outliers...median is more resistant
+ If more than one more...probably an usual distribution...like in the household pets example we looked at earlier

---
##  Central tendencies: Mean vs. median vs. mode

```{r tbl11, echo = FALSE}
tbl5 <- tibble::tribble(
~`Biology`, ~`Psychology`, ~`Sociology`,
"35,000", "22,000", "8,000",
"30,000", "19,000", "19,000",
"25,000", "30,000", "15,000",
"25,000", "24,000", "10,000",
"34,000", "25,000", "2,000,000",
"Mean: 29,8000", "Mean: 24,000", "Mean: 410,000",
"Median: 30,000", "Median: 24,000", "Median: 15,000"
)

kableExtra::kable_styling(knitr::kable(tbl5, format = "html", escape = FALSE, align = c("ccc")), font_size = 22)
```

.right[
<img src="assets/img/image6.jpg" height=150><br>
Christian Laettner
]

???

+ Use this as an example to illustrate the mean being pulled away from the median

---
##  Central tendencies: Mean vs. median vs. mode

<br>
$$[2, 2, 2, 2, 2, 3, 3, 3, 3, 3.5, 3.5, 3.5, 4, 4, 4, 4, 4, 4, \\ 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5.5, 5.5, 5.5, 5.75, 6, 6, 7]$$

<br>
<br>
$$\overline{X} = \frac{146.75}{35} = 4.19$$

<br>
<br>
$$Median = 4$$

<br>
<br>
$$Mode = 5$$

???

+ So know that we know about the mean, median, and mode....and how they each does us something different about the data they are describing...
+ What do the measures of central tendency tell us about our class screen time data
  + How might we describe the typical screen time use among the students in this class?

---
#  Central tendencies: Data types

<br>
<br>
.center[<img src="assets/img/image7.png" width=650>]

???

+ Which level of central tendency we use depends on how the data are measured
+ Explain table
+ This is another reason why we say interval/ratio data contain more information
