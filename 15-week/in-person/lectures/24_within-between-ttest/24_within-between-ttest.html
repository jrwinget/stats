<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
  <head>
    <title>Within- vs. Between-Groups T-Tests</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 304, Winget" />
    <link href="libs/remark-css-0.0.1/default.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/default-fonts.css" rel="stylesheet" />
    <link href="libs/remark-css-0.0.1/metropolis-fonts.css" rel="stylesheet" />
    <script src="libs/kePrint-0.0.1/kePrint.js"></script>
    <link rel="stylesheet" href="my-style.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Within- vs. Between-Groups T-Tests
### PSYC 304, Winget

---

#  Within vs. between-groups t-test

+ There are two more types of t-tests left to discuss
  + Within-groups and between-groups t-tests


+ In both of these cases, we are no longer comparing a single mean to the population! 
  + It can be done even if you have no information about the population


+ Therefore, these tests are very handy for real research


+ I will give a conceptual overview of both, with slightly more emphasis on between-groups t-tests


+ Slightly less emphasis on computing from raw data: The book covers computing from raw data extensively
  + Lecture will only give a brief summary of the computations

???

+ Check Allison Horst github for pictures

---
#  Within-groups t-test

+ Used when you want to compare scores from the same sample under one condition to scores under another condition
  + e.g.  People’s depression scores before therapy and after therapy (pre-test post-test design)
  + e.g., How well a group of kids behaved when they were being supervised by Mr. Davis vs. Ms. Jones
  + e.g., Road deaths in cities before and after new speed limits are enacted 
+ ONLY two conditions – not more! 
  + If more, you need a different kind of test
+ Goes by many names
  + AKA dependent-samples t-test: One set of scores depends on another
  + AKA paired-samples t-test: Pairing Time 1 score to a Time 2 score for the same subject
+ Compare difference between means to `\(s_\overline{X}\)` (standard deviation of the t-distribution)

???

+ 

---
#  Within-groups t-test

+ Example research hypothesis `\((H_1)\)`: Caffeine improves memory


???

+ What would be the null hypothesis?

--

+ `\(H_0\)`  = Caffeine has no effect or a negative effect on memory 


+ Example research situation: You have 5 people, measure their memory skills (how many digits out of 100 can they memorize?). Give them all caffeine. Then, measure those same 5 people’s memory skills again (100 new digits. How many can they memorize?)
+ You compare their memory skills at Time 1 (first memory test) to Time 2 (post-caffeine)

.center[&lt;img src="assets/img/image1.png"&gt;]

---
#  Example: Within-groups t-test

&lt;br&gt;

| Participant | After caffeine memory &lt;br&gt; score (Time 2) | Before caffeine memory &lt;br&gt; score (Time 1) | Difference score &lt;br&gt; (Time 2 - Time 1) | 
|:---:|:--------------:|:--------------------:|:----------------------:|
| 1         | 30    | 20   | 10 |
| 2         | 30    | 20   | 10 |
| 3         | 20    | 20   | 0  |
| 4         | 40    | 25   | 15 |
| 5         | 30    | 25   | 5  |

&lt;br&gt;
.center[Mean of difference scores = .red[**8**]]

???

+ 

---
#  Example: Within-groups t-test

| Participant | Time 2 score | Time 1 score | Difference score, `\(D\)` | Mean difference score, `\(\overline{D}\)` | `\(D - \overline{D}\)` | `\((D - \overline{D})^2\)` |
|:---:|:--------------:|:--------------------:|:----------------------:|:---:|:---:|:---:|
| 1         | 30    | 20   | 10 | 8 | 2  | 4  |
| 2         | 30    | 20   | 10 | 8 | 2  | 4  |
| 3         | 20    | 20   | 0  | 8 | -8 | 64 |
| 4         | 40    | 25   | 15 | 8 | 7  | 49 |
| 5         | 30    | 25   | 5  | 8 | -3 | 9  |

.center[Sum of squared deviation scores (aka, "Sum of Squares" or SS) = .red[**130**]


`\(\overline{D} = (10+10+0+15+5) / 5 = 8\)` &lt;br&gt;
`\(s_{D}^2 = SS/(N - 1) = 130/4 = 32.5\)` &lt;br&gt;
`\(s_D = \sqrt{s_{D}^2} = \sqrt{32.5} = 5.70\)` &lt;br&gt;
`\(s_\overline{D}^2 = s_{D}^2/N = 32.5/5 = 6.5\)` &lt;br&gt;
`\(s_\overline{D} = s_D/\sqrt{N} = 2.55\)`
]

???

+ 

---
#  Example: Within-groups t-test

+ Figure out the critical t-value
  + `\(df = N -1\)` 
  + .red[Number of people] – 1, **not number of observations** – 1


+ As usual, look it up in the t-test table. You need to know: 
  + `\(\alpha = .05\)`
  + `\(df = 5 - 1 = 4\)`
  + One-tailed


+ In the table, you see that `\(t_{crit} = 2.132\)`

???

+ 

---
#  Example: Within-groups t-test

+ Figure out the `\(t\)` that represents your situation (in this case, the `\(t\)` that represents the magnitude of pre/post differences) 

`$$t_{obt} = \frac{\overline{D}}{s_D / \sqrt{N}}$$`

  + `\(t_{obt}\)` = (mean of the difference scores) / `\(s_\overline{D}\)`  
  + Mean of difference scores `\((D)\)` = 8 (see slide 5)
  + `\(s_\overline{D} = 2.55\)` (see slide 6)
  + `\(t_{obt} = 8 / 2.55\)`
  + `\(t_{obt} = 3.13\)`


+ `\(3.13 &gt; 2.132\)`, so .red[**reject the null hypothesis**]


+ In other words, the .blue[**difference scores**] were big enough “within this sample” that we conclude there is an effect of caffeine on memory, `\(p &lt; .05\)`

???

+ 

---
#  Within-groups t-test

+ With within-group t-tests, we are concerned with difference scores “within” the same units of observation (in this example, within the same *people*) 


+ The research hypothesis is that difference scores will be different from 0 (greater than, less than, or both, if two tailed). If people basically are scoring the same pre- and post-, difference scores will be close to zero


+ The null hypothesis is that the difference score are = 0 (No difference between Time 1 and Time 2)


+ The hypothesis-testing steps are very similar to one-sample t-test, we just use .blue[**difference scores**] (and a comparison distribution of difference scores)

???

+ 

---
#  Between-groups t-test

+ With the between groups t-test (a.k.a. independent-samples t-test), you compare two samples that estimate two populations (dichotomous) 
  + Canadian vs. US residents
  + Guinness in Ireland vs. Guinness outside of Ireland 
  + Private school vs. state school-educated people
  + Group that is given a new drug vs. control group
+ Just like with within-group studies, **you don’t need to know any population parameters!**
+ This is a very real-world statistical technique

&lt;br&gt;
.center[
&lt;table class="table" style="font-size: 20px; margin-left: auto; margin-right: auto;"&gt;
 &lt;thead&gt;
  &lt;tr&gt;
   &lt;th style="text-align:left;"&gt; Location &lt;/th&gt;
   &lt;th style="text-align:center;"&gt; Sample size &lt;/th&gt;
   &lt;th style="text-align:center;"&gt; Mean &lt;/th&gt;
   &lt;th style="text-align:center;"&gt; Standard deviation &lt;/th&gt;
  &lt;/tr&gt;
 &lt;/thead&gt;
&lt;tbody&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; Ireland &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 42 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 74 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 7.4 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; Elsewhere &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 61 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 57 &lt;/td&gt;
   &lt;td style="text-align:center;"&gt; 7.1 &lt;/td&gt;
  &lt;/tr&gt;
&lt;/tbody&gt;
&lt;/table&gt;
]

???

+ Now that we covered a bit of the history of the t-test, let me also let you in on a little secret...Guinness really does taste better in Ireland
+ I've never been to Ireland, so I can't attest to this from personal experience
+ But, the results of the Guinness Overall Enjoyment Score (GOES) has the t-test support this claim 
+ Using the GOES rating system, researchers from 4 different countries traveled to different pubs around the world collecting data on the taste of Guinness
  + Cuz...you know...science
+ By the end of their travels, pints consumed in Ireland received a mean GOES score of 74...while the average GOES score for Guinness tasted elsewhere was 57
+ Now...the question was...is this difference of 74 versus 57 significant, or is it simply due to natural, random variation? 
+ This question is perfect for a t-test
+ Using these means and an independent samples t-test, the researchers were able to reject the null hypothesis (of no difference) and conclude that in fact Guinness served in an Irish pub tastes significantly better than pints served elsewhere around the globe

---
#  Data viz: T-test

&lt;img src="24_within-between-ttest_files/figure-html/gph1-1.png" style="display: block; margin: auto;" /&gt;

???

+ Explain what the variables are
+ Point our error bars...represent the standard error of the mean

---
#  Between-groups t-test

+ Often used in research where there is a “control group”
  + People who are not given the drug
  + People who don’t get the mindfulness meditation training
  + People who are not induced to feel a certain emotion before completing a task


+ Since we don’t know population parameters, the “control group” stands in for the overall population – they are a sample of the general population


+ However, between-groups t-tests can also be used to make comparisons between “naturally occurring” groups:
  + Students vs. professors
  + Cycling commuters vs. car commuters, etc. 

???

+ 

---
#  Between-groups t-test

+ The dependent variable is continuous (i.e., ratio/interval)


+ As with within-groups, the independent variable is dichotomous (has only two “levels”: red vs. blue team; driving vs. cycling)
  + The IV is not continuous (e.g., commute time). IF you want to compare two continuous variables, refer back to correlation or regression
  + The IV, as you want to test it, does not contain more than 2 levels (e.g., dog vs. cat vs. bird; Prozac vs. therapy vs. control). Of course, you could pick an IV that naturally DOES have more than two groups, you just need to select only two groups from within that (e.g., Students vs. Professors; just don’t include staff, parents, etc., in this particular t-test)
      + If you want to compare more than 2 groups at a time, refer to our next topic (ANOVA)

???

+ 

---
#  Example: Between-groups t-test

+ A researcher thinks university students use their cell phones more often than university professors do 


+ Sounds like a between groups t-test question, because: 
  + Dependent variable is continuous: hours of cell phone use
  + Independent variable is categorical: student vs. professor
  + Independent variable has only two levels: student and professor


+ Say the researcher takes a sample of 5 students, and a sample of 6 professors. The students are on their phone an average of 3.00 hours a day. The professors are on their phone an average of 1.33 hours a day. 
  + `\(3.00 – 1.33 = 1.77\)`
  + Is this a significant difference?? 

???

+ 

---
#  Hypothesis testing: Steps

1. State the research and null hypotheses
1. Determine the characteristics of the comparison distribution &lt;br&gt; (t-distribution, sample size)
1. Determine the critical value (a critical t-value)
1. Compute the obtained statistic – the value that best represents the sample (an obtained 
t-value)
1. Comment on the null hypothesis

???

+

---
#  Step 1: State research and null hypothesis

+ In the previous problems, we typically let `\(\mu_2\)` just be the general population/comparison distribution Now, we don’t have a general population, we’re comparing two samples (which represent two populations). So, its useful to explicitly define your `\(\mu\)`'s
  + `\(\mu_1\)` = students’ mean cell phone usage time
  + `\(\mu_2\)` = professors’ mean cell phone usage time


+ `\(H_1: \mu_1 &gt; \mu_2\)`: Mean cell phone use time will be greater for students than for professors

???

+ + In the previous problems, we typically let `\(\mu_2\)` just be the general population/comparison distribution Now, we don’t have a general population, we’re comparing two samples (which represent two populations). So, its useful to explicitly define your `\(\mu\)`s
  + State `\(\mu\)`s
+ Notice here that even though we're using samples, the hypotheses is stated in terms of population parameters
  + This is because the decision we're making involves what is true about the population...not just the two specific samples in the study
+ What's the null hypothesis?

--

+ `\(H_0: \mu_1 \le \mu_2\)`: Mean cell phone use time for students will be less than or equal to cell phone use time for professors

---
#  Step 2: Describe comparison distribution

+ Describing the comparison distribution is complex for between-groups t-tests


+ For between-groups t-tests, you need to consider that the samples came from two populations: Students and professors
  + Sampling distribution that one sample came from (e.g., students)
  + Sampling distribution the other sample came from (e.g., professors)


+ You need to take into consideration the variance from both of those distributions and find the “pooled variance”

???

+ 

---
#  Step 2: Describe comparison distribution

+ The comparison distribution for between-subjects t-test is the sampling distribution of difference scores, based on degrees of freedom


+ **Degrees of freedom** = `\(N_1 + N_2 – 2\)`
  + Here, `\(df = 5 + 6 – 2  = 9\)`


+ In this example, the comparison distribution is a sampling distribution of difference scores, based on 9 degrees of freedom

???

+ 

---
#  Step 3: Determine critical t-value

+ Using the t-table (Appendix 2, Table B):
  + `\(\alpha = .05\)`
  + One tailed
  + `\(df = 9\)`


+ `\(t_{crit} = 1.833\)`

???

+ 

---
#  Step 4: Calculate observed t-value

+ Between-samples formula:

&lt;br&gt;
&lt;br&gt;
`$$t = \frac{(\bar{X_1} - \bar{X_2})}{s_{\bar{X}_1 - \bar{X}_2}}$$`

&lt;br&gt;
&lt;br&gt;
.center[
The difference between the two sample means &lt;br&gt;
--------------------*divided by*-------------------- &lt;br&gt;
The sampling SD of the difference scores
]


???

+ How to calculate t is extensively covered in chapter 10 of your book

---
#  Step 4: Calculate observed t-value

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
`$$t = \frac{(\bar{X_1} - \bar{X_2})}{s_{\bar{X}_1 - \bar{X}_2}}$$`

&lt;br&gt;
.center[
3 - 1.3333 &lt;br&gt;
-----*divided by*----- &lt;br&gt;
0.547

&lt;br&gt;
&lt;br&gt;
`\(t = 3.05\)`
]

???

+ 

---
#  Step 5: Comment on null hypothesis

+ `\(t_{crit} = 1.833\)`


+ `\(t_{obt} = 3.05\)`


+ `\(3.05 &gt; 1.833\)`


+ We .red[**reject the null**] hypothesis that students’ cell phone use is equal to or less than professors’ cell phone use

???

+ 

---
#  Equality of variances

+ Student’s t-test assumes that the variance of both of your samples is equal
+ If it isn’t, you can’t as easily “pool” the variance of the two samples
+ If Levene’s test was significant `\((p &lt;.05)\)`, the obtained differences in sample variances are unlikely to have occurred based on random sampling from a population with equal variances
  + So, we .red[**DO NOT**] want a significant value here

&lt;br&gt;



```r
library(car)

# Levene's test
leveneTest(grade ~ teacher, data = grades_between)
```

```
## Levene's Test for Homogeneity of Variance (center = median)
##       Df F value Pr(&gt;F)
## group  1  0.0034 0.9537
##       38
```


???

+ Equal variances across samples is called homogeneity of variance
+ Some statistical tests, like the t-test, assume that variances are equal across groups or samples
+ The Levene test can be used to verify that assumption
+ The t-test (and other statistical tests) assume that although different samples can come from populations with different means, they have the same variance
+ Levene's test tests the null hypothesis that the population variances are equal...called homogeneity of variance or homoscedasticity
+ If the resulting p-value of Levene's test is less than some significance level (typically 0.05)...the obtained differences in sample variances are unlikely to have occurred based on random sampling from a population with equal variances
+ Thus, the null hypothesis of equal variances is rejected and it is concluded that there is a difference between the variances in the population
+ More details about equality of variances can be found in the textbook

---
#  Major takeaways

+ Hypothesis testing follows the same basic pattern regardless of the hypothesis you are testing


+ You need to know what is being compared to what, and you need to use the appropriate equations and critical values


+ But the logic remains the same – if `\(t\)` or `\(z\)` or `\(r\)` is big enough, then reject the null, otherwise fail to reject the null


+ Always remember to lay out the null and alternative hypotheses, and the equation for the observed value, and the appropriate critical value
  + The rest is just plugging the numbers in carefully to the appropriate equation

???

+ 

---
#  Major takeaways

+ There are three types of t-tests, and they all have different purposes and make different assumptions. Be able to figure out which one a research question calls for
  + One-sample t-tests compares a sample to the population
  + Within-samples t-tests compares scores from the same person under different conditions (for example, before-after); and test whether the differences are significantly different from zero
  + Between-samples t-test compare the means of two different groups


+ t-distributions vary based on sample size: the bigger your sample size, the easier it is to detect real effects


+ The issues of null vs. research hypotheses, Type I &amp; Type II errors, and alpha level apply to t-tests just the same as to z-score testing

???

+
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
