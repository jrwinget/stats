library(psych)
library(car)
library(tidyverse)

# load practice data
grades_within <- read_csv("./t-test/grades_within.csv")

# examine data
glimpse(grades_within)
describe(grades_within)

# ONE SAMPLE T-TEST

grades_within %>% 
  summarize(
    mean_grade = mean(grade),
    sample_size = n()
  )

# comparing grade sample mean to grade population mean of 22
# H0: sample > or = 22, H1: sample < 22
t.test(grades_within$grade, mu = 22, alternative = "less")

# same data, but what if we predicted the sample would be higher than the population?
# H0: sample < or = 22, H1: sample > 22
t.test(grades_within$grade, mu = 22, alternative = "greater")

# PAIRED SAMPLES T-TEST

grades_within %>% 
  group_by(time) %>% 
  summarize(
    mean_grade = mean(grade),
    sample_size = n()
  )

# comparing difference of pre grade sample mean and post grade sample mean
# formula structure: t.test(y ~ x, data = data-name)
# H0: sample1 = sample2, H1: sample1 does not equal sample2
t.test(grade ~ time, data = grades_within, 
       paired = TRUE, alternative = "two.sided")

# same data, but what if we predicted the sample would be higher than the population?
# without specifying alternative, t.test() will automatically run "two.sided"
# H0: sample1 = sample2, H1: sample1 > sample2
t.test(grade ~ time, data = grades_within, 
       paired = TRUE, alternative = "greater") # notice what happens to the p-value

#################################################################################
# LEARNING CHECK                                                                #
# What is the differences between some of the t-tests we just ran?              #
# Which mean was higher in the last paired samples t-test?                      #
#################################################################################

# load practice data
grades_between <- read_csv("./t-test/grades_between.csv", 
                           col_types = "dfd")

# INDEPENDENT SAMPLES T-TEST

grades_between %>% 
  group_by(teacher) %>% 
  summarize(
    mean_grade = mean(grade),
    sample_size = n()
  )

# comparing difference of teacher 1 grade sample mean and teacher 2 grade sample mean
# formula structure: t.test(y ~ x, data = data-name)
# H0: sample1 = sample2, #H1: sample1 does not equal sample2
t.test(grade ~ teacher, data = grades_between,
       alternative = "two.sided")

# same data, but what if we predicted the sample would be greater than the population?
# without specifying alternative, t.test() will automatically run "two.sided"
# H0: sample1 < or = sample2, H1: sample1 > sample2
t.test(grade ~ teacher, data = grades_between,
       alternative = "greater") # notice what happens to the p-value

# test for equality of variances
# structure: leveneTest(y ~ x, data = data-name)
leveneTest(grade ~ teacher, data = grades_between)

#################################################################################
# LEARNING CHECK                                                                #
# How would you interpret the output from the independent samples t-test?       #
#################################################################################

# bar plots are among the most common ways to visualize t-test results
# here, we graph student exam score mean for each teaching style sample
ggplot(data = grades_between,
       mapping = aes(x = teacher,
                     y = grade)) +
  stat_summary(geom = "bar", 
               fun.y = mean, # uses means in the graph instead of freq counts
               position = "dodge", 
               fill = "steelblue") +
  stat_summary(geom = "errorbar", # adds error bars
               fun.data = mean_se, # uses standard error of the mean for error bars
               position = "dodge") +
  labs(title = "Student grades based on teaching style",
       x = "Teaching style",
       y = "Exam score")
