<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Statistical Power</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 304, Winget" />
    <script src="libs/header-attrs-2.7/header-attrs.js"></script>
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Statistical Power
### PSYC 304, Winget

---




#  Introduction

+ Hypothesis testing is an imperfect process
  + There's always a chance we're wrong about our conclusions (Type I and Type II errors)


+ Statistical power
  + Probability of rejecting the null hypothesis when, in fact, the null is false and the research hypothesis is true

???

+ We've talked about how hypothesis testing is an imperfect endeavor...we always leave some chance we're wrong or that the results are simply due to chance
+ The best we can do is reject the null hypothesis and feel reasonably confident we've made the right choice
+ But Type I and Type II errors are always possible...so we can never prove anything and we can never be 100% sure about our conclusions
+ Not trying to be a pessimist...but there are problems with the process that we need to be aware of
+ However, in today's lecture...we'll focus on a solution to this problem
+ And this solution revolves around the concept of statistical power
  + Probability of rejecting the null hypothesis when, in fact, the null is false and the research hypothesis is true
+ So, you may have a situation in which your research hypothesis is objectively true...but this is a separate issue from how likely it is that the hypothesis testing procedure will find that the research hypothesis is supported
  + This is a subtle difference when first hearing about it...but this nuance sits at the heart of the issue of statistical power

---
#  What is statistical power?

+ Allows us to say: 
  + ~~We're right! We rejected the null hypothesis, so the research hypothesis is definitely right!~~
  + Not only did we reject the null hypothesis, we can also be very confident that we made the correct decision given the high amount of power that we had in the design!


+ Power is considered *.black[before]* you actually conduct a study
  + **Definition:** Probability of rejecting the null hypothesis when, in fact, the null is false and the research hypothesis is true
  + **General idea:** The likelihood of detecting an effect if there's something to be found

???

+ With power, instead of asking whether we're correct or not in our hypothesis testing procedure...we instead ask how likely it is that we're able to find that we're correct (by rejecting the null hypothesis) if, in fact, the null hypothesis is false
+ In other words, instead of saying "we're right--we rejected the null hypothesis--the research hypothesis is definitely right"...we can say something like "not only did we reject the null hypothesis, we can also be very confident that we made the correct decision given the high amount of power that we had in the design"
+ I'll admit, this wording is nuanced...but it's clear and strong--and it matches the best we can do in making the case that we've done well in working with the hypothesis-testing procedure
+ Power is often considered before you conduct a study
+ It has less to do with whether the question you have is correct...and more to do with the research design and how well that methodology is designed to uncover any effects that actually exist
+ In lay terms, its basically the likelihood of detecting an effect if there's something to be found

---
#  Essential concepts

1. The null hypothesis, `\(H_0\)` &lt;br&gt;&lt;br&gt;
1. Alpha level, `\(\alpha\)` &lt;br&gt;&lt;br&gt;
1. Type I error &lt;br&gt;&lt;br&gt;
1. Type II error

???

+ Statistical power relies on 4 different concepts that we've already encountered

---
#  Review: Decision table

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;

|                          | `\(H_0\)` is true  | `\(H_0\)` is false |
|:------------------------:|:--------------:|:--------------:|
| **Reject `\(H_0\)`**         | **Type I error** &lt;br&gt; "False alarm" &lt;br&gt; Probability = `\(\alpha\)`  | **.green[Correct decision!]** &lt;br&gt; Correctly detected a real effect &lt;br&gt; Probability = `\((1 – \beta)\)` |
| **Fail to reject `\(H_0\)`** | **.green[Correct decision!]** &lt;br&gt; Correctly detected no effect &lt;br&gt; Probability = `\((1 – \alpha)\)` | **Type II error** &lt;br&gt; "Miss" &lt;br&gt; Probability = `\(\beta\)` |

???

+ Explain correct decisions
+ When `\(H_0\)` is true and you reject it, you make a Type I error
  + When there really is no effect, but the statistical test comes out significant by chance, you make a Type I error
  + When Ho is true, the probability of making a Type I error is called alpha (α)
  + This probability is the significance level associated with your statistical test
+ When `\(H_0\)` is false and you fail to reject it, you make a Type II error
  + When, in the population, there really is an effect, but your statistical test comes out non-significant, due to inadequate power and/or bad luck with sampling error, you make a Type II error
  + When Ho is false, (so that there really is an effect there waiting to be found) the probability of making a Type II error is called beta (β)

---
#  An example of power

+ Let's say you work in the Office of Institutional Research (OIR)


+ Supervisor asks you to conduct a study to see if graduates from your college tend to score higher than the national average on the TMUJSI
  + `\(\mu = 25\)`
  + `\(\sigma = 5\)`


+ Money is tight, so you can only collect a random sample of 20 graduates
  + Use a one-tailed test, `\(\alpha = .05\)`
  + `\(\bar{X} = 26\)`
  

+ Does this hypothesis test even stand a chance?

???

+ Suppose you graduate with your degree and you get a job at a university's OIR
  + Office found on most college campuses that conducts research on issues related to the college (e.g., issues of grades, graduation rates, etc.)
+ Your supervisor comes to you ans says she wants you to conduct a study to see if graduates from your college tend to score higher than the national average on a standardized test of job satisfaction (TMUJSI - the made-up job satisfaction inventory)
+ This test has been given to millions of Americans and the best estimates of the population mean `\((\mu)\)` and standard deviation `\((\sigma)\)` are 25 and 5, respectively
+ Money is tight, and your supervisor tells you that there's only enough budget for you to collect data from 20 randomly sampled graduates
+ You're asked to use a standard alpha level of .05 and to assume a one-tailed test...with the research hypothesis being that graduates from your college score higher than the national average
+ So you collect the data from your 20 randomly sampled graduates, and compute the mean of the sample's TMUJST score and find it's 26
+ You pause...not knowing how your hypothesis test will go
+ You know that 26 is not much higher than 25, which was the national average
+ So, you wonder if your hypothesis test even has a fighting chance
+ Suppose the research hypothesis is true...that graduates from your college really do score higher than the national average
  + With an N of 20 and a difference between the means of only one unit, do you feel like your first task in this new job is going to succeed?
+ All of these questions pertain to the issue of estimating statistical power
  + Given all the information you have, **before you even conduct the hypothesis test**, you'd like to get a sense of how likely it is you'll reject the null hypothesis
+ So as you can see, power (even though it's somewhat abstract) is very applicable to real-world situations that matter

---
##  Step 1: Articulate the null and research hypotheses

+ `\(\mu_1\)`: The population mean for the special population (graduates of your college) on TMUJSI
+ `\(\mu_2\)`: The population mean for the general population of college graduates on TMUJSI

&lt;br&gt;
`$$H_1: \mu_1 &gt; \mu_2$$`
`$$H_0: \mu_1 \le \mu_2$$`

&lt;br&gt;
+ In other words, the population mean for the special population is predicted to be greater than the population mean for the general population

???

+ 

---
###  Step 2: Draw the comparison distribution

+ Mean of the sampling distribution
  + `\(\mu_\bar{X} = \bar{X} = 25\)`
+ Standard error of the mean:
  + `\(\sigma_\bar{X} = \frac{\sigma}{\sqrt{N}} = \frac{5}{\sqrt{20}} = 1.12\)`

&lt;br&gt;
&lt;img src="02_power_files/figure-html/gph1-1.png" style="display: block; margin: auto;" /&gt;


???

+ You're not going to conduct a hypothesis test--that's not the point of estimating power
+ But if you *WERE* going to carry out a hypothesis test, what would be the comparison distribution?
  + It would be the distribution corresponding to the general population
  + And importantly, it would be a distribution of means corresponding to the general population
  + Remember the idea of sampling distributions...they still apply here!
+ So the comparison distribution will be a distribution of means corresponding to the general population--taking the N of the sample (20, in this case) into account
+ The mean for this distribution is represented by the mean of the sampling distribution...which in this case is equal to the sample mean of our 20 graduates
+ The standard deviation of the sampling distribution...known as the standard error of the mean...is represented by `\(\sigma_\bar{X}\)`
  + We already know the population SD from the opening example, which was 5, so we plug that in...
+ And so now, we just have to draw the comparison distribution, which will be a normal distribution of means, with a mean of 25 and standard error of 1.12
  + When you draw this, give yourself lots of room because we will be drawing another distribution on top of it it
  + Which is why this distribution is labeled the lower distribution

---
###  Step 3: Mark `\(Z_{crit}\)` on the comparison distribution

&lt;img src="02_power_files/figure-html/gph2-1.png" style="display: block; margin: auto;" /&gt;

???

+ You're not going to carry out the hypothesis test, but if you were you'd need to demarcate the `\(Z_{crit}\)` score
+ Recall that this sets the alpha region (or the rejection region)
+ Also recall that in this situation, our alpha level is .05 and we're using a one tailed test
+ The mean for the special population (the 20 graduates) is predicted to be larger than the mean of the comparison distribution
+ So if alpha is 5% of the distribution and is on the right of the mean in this case (bc it's higher than the mean), then it refers to all points that are to the right of the `\(Z_{crit}\)` score
  + And as we know, the `\(Z_{crit}\)` is based on alpha and the fact that it's a one tailed test
+ So if 5% of the distribution is greater than the `\(Z_{crit}\)` score, then 45% is between the `\(Z_{crit}\)` score and the mean
  + So, we look up the mean to Z proportion in Appendix 2, table A (since we're using z-scores)...and look up the z-score associated with a mean to z proportion of 45%
  + In this case, we find the z-score is 1.64
  + So we need to demarcate this on our comparison distribution

---
###  Step 4: Shade the alpha region

&lt;img src="02_power_files/figure-html/gph3-1.png" style="display: block; margin: auto;" /&gt;


???

+ Again, you're not going to carry out the hypothesis test, but if you were, you'd need the alpha region shaded
+ The alpha region in this case is the entire section to the right of `\(Z_{crit}\)`

---
###  Step 5: Draw the upper distribution

+ Homogeneity of variances assumed to be true
  + So `\(\sigma_\bar{X_1} = \sigma_\bar{X_2} = 1.12\)`

&lt;img src="02_power_files/figure-html/gph4-1.png" style="display: block; margin: auto;" /&gt;

???

+ The upper distribution corresponds to the special population...in this case the 20 randomly sampled graduates
+ So the sample mean for the special population in this case is 26
+ Because this number is greater than the mean for the comparison distribution, the entire distribution (which you'll draw above the lower distribution we just created) will be to the right of the lower distribution
+ Here, we'll go with the assumption of homogeneity of variance...assuming that the variances and standard deviations for the two distributions are equal to one another

---
##  Step 6: Compute and mark `\(\bar{X}_{crit}\)`

&lt;br&gt;
`$$X = z(s) + \bar{X}$$`

&lt;br&gt;
&lt;br&gt;
`$$\bar{X}_{crit} = z_{crit}(\sigma_\bar{X_2}) + \mu_\bar{X_2}$$`

&lt;br&gt;
&lt;br&gt;
`$$\bar{X}_{crit} = 1.64(1.12) + 25$$`

&lt;br&gt;
&lt;br&gt;
`$$\bar{X}_{crit} = 26.84$$`

???

+ Ultimately, we'll need to see what percentage of the upper distribution overlaps with the alpha region in the lower distribution
+ In a strict, mechanical sense, that is actually power...the percentage of the upper distribution that is extreme enough to reject the null hypothesis on the lower distribution
+ So, we need a mechanism to compare across the two distributions
+ The critical value on the lower distribution needs to be understood in context of the upper distribution
+ On the lower distribution, we know this point in terms of z-critical
  + But a z-score is always relative to a particular distribution (it uses that distributions mean)
  + Z-crit is 1.64 SD above the mean on the lower distribution, but that same score may be a different number of SD away from the mean on the upper distribution
+ So we need to figure out the raw score of z-critical
+ Here, we use the z-score conversion formula we used awhile ago
+ However, we're now in a situation where the raw score we're computing isn't X, but rather, the critical sampling distribution mean
  + In other words, we're computing the z-critical score represented as a raw score
+ So in our example...
+ But it's all the same logic we've seen before

---
##  Step 7: Compute `\(\bar{X}_{upper}\)`

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
`$$\bar{X}_{upper} = \bar{X}_{crit}$$`

&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
`$$\bar{X}_{upper} = 26.84$$`

???

+ Now, we need to think about that sample point...which sets the alpha region on the lower distribution...in terms of the upper distribution
+ This may sound sill or simplistic, but it's useful to think of `\(\bar{X}_{upper}\)` as being the critical raw score of the lower distribution
+ So in this case, the upper mean is equal to 26.84
+ We can now think of that same score (26.84) in terms of both distributions
+ On the lower distribution, this point is the critical raw score of the sampling distribution...or the critical z-score expressed as a raw score
+ And on the upper distribution, its `\(\bar{X}_{upper}\)`

---
##  Step 8: Compute `\(Z_{upper}\)`

`$$z = \frac{(X - \bar{X})}{s}$$`

&lt;br&gt;
&lt;br&gt;
`$$z_{upper} = \frac{(\bar{X}_{upper} - \mu_\bar{X_1})}{\sigma_\bar{X_1}}$$`

&lt;br&gt;
&lt;br&gt;
`$$z_{upper} = \frac{(26.84 - 26)}{1.12}$$`

&lt;br&gt;
&lt;br&gt;
`$$z_{upper} = \frac{(.84)}{1.12} = .75$$`

???

+ We're ultimately working toward figuring out what percentage of the upper distribution is extreme enough to reject the null hypothesis on the comparison distribution (lower distribution)
+ To figure out percentages of sections in a normal distribution, we need to work with z-scores and use the standard normal table in appendix 2
+ So, we need to figure out what `\(\bar{X}_{upper}\)` is as a z-score
+ Remember z-scores are only relevant to a particular distribution
+ Here, we're interested in how `\(Z_{upper}\)` is relative to the mean of the upper distribution
+ Therefore, the same raw score of 26.84 will have a different Z value on the lower distribution than it will in the upper distribution
+ So while this value corresponds to a z-score of 1.64 on the lower distribution, it will have a different value (called `\(Z-{upper}\)`) on the upper distribution
+ Once we have this z-score, we can work on computing power
+ To compute this z-score, we use a modified version of the z formula 

---
##  Step 9: Connect `\(Z_{upper}\)` and `\(Z_{crit}\)`

&lt;img src="02_power_files/figure-html/gph5-1.png" style="display: block; margin: auto;" /&gt;
&lt;img src="02_power_files/figure-html/gph6-1.png" style="display: block; margin: auto;" /&gt;


???

+ To figure out what percentage of the upper distribution overlaps with the lower distribution, we'll need to place the distributions appropriately in space, and then draw a line connecting z-critical on the lower distribution to z-upper on the upper distribution
+ One the slide, just imagine the 2 red lines are connected
+ This allows us to visually see which section of the upper distribution overlap with the alpha region of the lower distribution
+ Note, when you're doing this by hand, it's pretty difficult to set things up so that you have a perfect straight line between the two distributions
  + This isn't completely necessary--the important thing is that the line that connects the two distributions connects z-upper and z-critical
+ When doing this by hand, it's okay if this line curves a bit
+ The goal here isn't to get a straight line--it's to connect the appropriate points in the two distributions
  + Having a straight line just makes things easier to visualize

---
###  Step 10: Shade power in upper distribution

&lt;img src="02_power_files/figure-html/gph7-1.png" style="display: block; margin: auto;" /&gt;
&lt;img src="02_power_files/figure-html/gph8-1.png" style="display: block; margin: auto;" /&gt;

???

+ Power is the percentage of the upper distribution that is extreme enough to reject the null hypothesis on the lower distribution
+ What percentage of the upper distribution overlaps with the alpha region in the lower distribution?
+ Looking at the graph, we can see this corresponds to all of the upper distribution to the right of z-upper
+ Given that z-upper is about in the middle of the right half of the upper distribution, everything to the right of this point overlaps with the alpha region
  + Power is the entire part of the upper distribution that is to the right of z-upper
+ So, we should now shade this area

---
###  Step 11: Compute power based on shaded section of upper distribution

&lt;img src="02_power_files/figure-html/gph9-1.png" style="display: block; margin: auto;" /&gt;

???

+ To compute power, we'll use the standard normal table in appendix 2
+ Let's first divide the upper distribution into 3 sections: A, B, and C
  + A, portion to left of mean
  + B, portion between mean and z-upper
  + C, shaded region, power
  + basically, z-upper divides the right of the distributions into sections b and c
+ Recall mean to z proportion only gives us section b...so we have to look up the z-score of interest (.75 - z-upper)
  + This will give us the proportion for section B
+ We can then subtract this amount from 50% and we'll have the answer we've been looking for...the percentage of the upper distribution that is in section C
+ According to the standard normal table...a z-score of .75 has 27.34% of the distribution between itself and the mean...this is the proportion for section B
+ To figure out what section C is...we take 100% subtract 50% for section A...which leaves us with 50%...and then we subtract section B from this
  + So 50% - 27.34% for section B leaves us with 22.66% for section C
+ Therefore, power is equal to 22.66%
+ Which means, if you were to conduct the hypothesis test, you'd have a 22.66% change of rejecting the null hypothesis
  + Not really that great...especially considering that power is suggested to be at least 80% if not 90%

---
##  Step 12: Compute `\(\beta\)`

&lt;br&gt;
&lt;img src="02_power_files/figure-html/gph10-1.png" style="display: block; margin: auto;" /&gt;

???

+ Now that we know power, the last thing we need to do is calculate beta
+ Beta, in this case, is literally the probability of a Type II error...and it's directly related to power
+ If power is the probability of correctly rejecting the null hypothesis, then Beta is the complement of this
+ To compute it, we take 100% and subtract power
+ So in our case, Beta is equal to 100% - 22.66%, which leaves us with 77.34%
+ This means, we have roughly a 77% change of committing a Type II error...missing an effect if it was actually there!
+ In other words, when power is low, Type II errors tend to be high

---
#  Conventions and decisions about power

+ Acceptable risk of a Type II error is often set at 1 in 5
  + A probability of 0.2 `\((\beta)\)`


+ The conventionally uncontroversial value for "adequate" statistical power is therefore set at `\(1 - 0.2 = 0.8\)` (or 80%)


+ People often regard the minimum acceptable statistical power for a proposed study as being an 80% chance of an effect that really exists showing up as a significant finding

???

+ 

---
#  Factors that influence power

+ One vs. two-tailed tests


+ Effect size (generally) and Cohen's d (in particular)


+ Population variability


+ Sample size

???

+ There are a number of things that influence power
+ One has to do with the specific form of hypothesis tests
+ Generally speaking, one tailed tests are more powerful than two tailed tests
  + Due to the fact that specific rejection regions are half as big with one-tail tests than two-tailed tests....because a two-tailed test divides the alpha across two rejection regions
+ Second, a larger effect size generally corresponds to more power
  + This is because large effect sizes indicate larger mean differences
  + And when mean differences are larger, the means between the comparison distributions are further apart...which makes it easier to detect an effect
+ Third, population variability can influence power by creating more noise between the means
  + In general, lower levels of population variability make correspond to more power
+ Lastly, and probably the easiest to control, is the sample size
  + This is related to the fact that lower variability increases power
  + Recall that the larger the sample we collect, the lower the variability tends to be
  + This was a feature of sampling distributions and the central limit theorem
  + So as N increases, power tends to increase
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
