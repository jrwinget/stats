---
title: "An introduction to R/RStudio"
author: "PSYC 304, Winget"
output:
  xaringan::moon_reader:
    css: xaringan-themer.css
    lib_dir: libs
    nature:
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---

```{r xaringan-themer, include=FALSE, warning=FALSE}
library(tidyverse)
library(xaringanthemer)

style_mono_light(
  base_color = "#E04556",
  text_color = "#7F7F7F",
  background_color = "#F2F2F2",
  title_slide_text_color = "#FFFFFF",
  text_slide_number_color = "#F2F2F2",
  colors = c(
    blue = "#0000FF",
    purple = "#800080"
  ),
  extra_css = list(
    ".large-text" = list("font-size" = "150%"),
    ".small-text" = list("font-size" = "75%")
  )
)
```

```{css, echo=FALSE}
pre {
  white-space: pre-wrap;
}
```

#  Today's goals

.pull-left[
<br>
+ Talk about what R and RStudio are


+ Introduce RStudio Cloud and set up account
  + Website we will use to run R/RStudio in this course


+ Discuss some basic programming approaches used in R
]

.pull-right[
<img src="assets/img/image23.png">
]

???

+ When mentioned RStudio Cloud...this is a website we will use to run R/RStudio in this course
  + This isn't how people typically use R/RStudio...rather, it's a cloud service designed for teaching R/RStudio
  + If you're interested in setting up R/RStudio on your own computer for long-term us outside of the coruse, email me and I can show you how to do so

---
#  What are R & RStudio?

+ R is free open source software for statistical computing and graphics
  + It's a computer language, much like Python, C, C++, etc., but it's specifically made for data analysis


+ RStudio is an app that interfaces with R to make it more flexible/powerful
  + We won't even scratch the surface of it's capabilities this semester
  + It also makes R easier and more intuitive

.center[<img src="assets/img/image24.png">]

???

+ So....what actually is R and how is it different than RStudio?
+ Well, R is free open source software for statistical computing and graphics
  + It's actually it's own computer language, much like Python, C, C++, etc., but it's made specifically for data analysis
+ RStudio is basically an app that interfaces with R that makes R much more flexible/powerful
  + So powerful that we won't even scratch the surface of it's capabilities this semester
  + It also makes R easier and more intuitive to use

---
#  Why R?

.pull-left[
+ FREE!


+ Open source


+ Reproducibility


+ Vast array of analytic methods
  + Power analysis, SEM, meta-analysis, quantitative text analysis, and more
]

.pull-right[
+ Handles all data types


+ New methods sooner


+ Incredible data viz options


+ Can also be used to create slides, research manuscripts, websites, and more!
]

.right[<img src="assets/img/image33.png" width=30%>]

???

+ First benefit is that it's completely free and runs on all computer platforms
  + No more licenses to worry about or computer rooms to reserve
+ It's also open source, which means you can see the actual code that was written to produce your results
  + You can even modify this code for your own needs if you wish
  + Makes it easier to understand what's going on and what the program is doing
+ Using open source software is making data and analyses more available and reproducible
  + Makes it more accessible to people that might not be able to buy commercially available software
  + This increases the changes that any potential errors in your analyses will be caught and corrected
+ There's also a ton of analytic methods you can use in R
  + Covers all of the basic stats that we will discuss in this course
  + But R also offers SEM, machine learning, deep learning, network analysis, and other methods
+ R is also very flexible in the type of data it can analyze
  + Many programs...like SPSS...save data in a specific way...and will only load data from a specific file type
  + R can handle data from SPSS files, from Excel files, from CSV files, SAS files, and hundreds of other types
  + If you can think of a type of data, there's a way to get it into R
+ Because R is so flexible in the methods and data types it can handle, R offers new methods sooner as well
  + Since people who develop new analytic methods often program in R...you'll often get access to them years before the methods are added to SPSS or SAS
+ R's graphics are extremely flexible and look SO much nicer than other programs' graphics
  + They're also more flexible...and you have much more control over how they look
+ There are other benefits too
    + Whereas many programs can only be used for statistical analysis...R can also be used to also write research reports in it (exports to word, pdf)
    + Can even handle track changes with certain packages
    + Can create slides/presentations with it (can export to powerpoint!)
    + It can sync with other software (like a library manager) and automatically import references
    + Takes a lot of human error out of the process because results are directly imported to those other areas from R

---
##  R in industry

.center[<img src="assets/img/image25.png" height=530>]


???

+ Becoming universal language for data analytic & data science

---
##  R in industry

.center[<img src="assets/img/image26.png" height=530>]


???

+ Becoming universal language for data analytic & data science

---
##  R in research

.center[<img src="assets/img/image27.png" height=530>]

???

+ Becoming universal language for data analytic & data science

---
##  R in research

.center[<img src="assets/img/image28.png" height=530>]

???

+ Becoming universal language for data analytic & data science

---
#  RStudio Cloud

+ Use the link in this week's module to access our class space on RStudio Cloud

.center[<img src="assets/img/image34.png" width=85%>]

???

+ It will bring you to a screen like you see here
+ Show your screen at this point
  + Show Sakai -> Cloud -> account -> join space -> projects (at top)
  + As R homework assignments come up...they will show up here
  + So the homework link in Sakai for those weeks will take you to RStudio Cloud, rather than an assignment page
  + When you get here, you'll see a project related to that week's homework assignment
  + Click on start, wait for the project to load, and you will be in an RStudio session
  + (show them around)
  + When working a hw assignment, just directly edit the files...(enter your name in the ex. script)
  + When you are finished working, make sure to save your work
  + Then, you can log out and come back to the assignment if you wish...and your progress will be saved
  + To submit your RStudio homework assignments...simply save your work on RStudio Cloud before the due date of the assignment
  + When grading, I will enter your score in the gradebook on Sakai, but I will provide feedback directly in your files on RStudio cloud
  + More details on this as we get closer to our first RStudio assignment
  + For now, I just want you to familiarize yourself with the platform

---
#  Using R/RStudio

+ Every action (i.e., command) you want to take in RStudio needs to be entered in the console or executed from a script file
  + It may take some time getting used to this, especially if you are used to point-and-click software
  + I will automate most things for you, but let me know if the concepts aren't connecting


+ Commands in R are made up of 2 parts
  + Objects
  + Functions

???

+ 

---
#  Using R/RStudio

+ Object
  + Anything created in R
  + Variable, collection of variables, a statistical model, etc.


+ Function
  + Things you do in R to create objects or obtain results


+ Thus, the general form of a command is
  + `Object <- function`
  + This reads as “object is created from function”


+ To execute command in **.blue[console]**, simply hit enter
  + To execute command from **.blue[script]**, highlight the code, hold command, and hit enter (or highlight and click run)  

???

+ 

---
#  Using RStudio

```{r}
rolling_stones <- c("Mick", "Keith", "Brian", "Bill", "Charlie")
```

<br>
.center[
`rolling_stones` = **.blue[object]** <br>
`c("Mick", "Keith", "Brian", "Bill", "Charlie")` = **.blue[function]**
]

<br>
+ Creates object called `rolling_stones` with is made up of the original band members' names
  + Used the `c` (concentrate) function, which groups things together
  + Wrote each members name, and by enclosing them in `c()`, we bind them into a single object
  + Entering this into the console and hitting enter creates the object and stores it in memory for later use

???

+ For example...

---
#  Using RStudio

+ Using objects
  + Type name of object into console to display its contents
  + R is case-sensitive, so if you used all lowercase, be consistent

<br>
<br>
```{r}
rolling_stones
```

???

+ 

---
#  Using R/RStudio

```{r}
rolling_stones <- rolling_stones[rolling_stones != "Brian"]
rolling_stones
```

<br>
+ This means that we're re-creating our object
  + Function says “use object `rolling_stones` but get rid of (`!=`) Brian"
  + `rolling_stones` now only contains 4 members

???

+ Don't worry about what we're doing to get rid of Brian here...focus on the distinction between the function and the object
+ We will spend more time going over proper procedures for stats later

---
#  Using R/RStudio

+ Let's add Mick Taylor to the band

<br>
```{r}
rolling_stones <- c(rolling_stones, "Mick")
rolling_stones
```

<br>
+ Executing this command shows us that The Rolling Stones now has a new guitarist

???

+ We can also manipulate objects we've created
+ 
+ Don't need to understand the syntax yet, just the general idea of object and functions

---
#  Scripts

+ While you can execute commands from the console, it's better to use the R editor and execute from there


+ A document of commands written in the editor is known as a script


+ Benefits:
  + You can save the script and rerun the exact same analyses at a later date
      + Fast rerunning of analyses, reproducible 
  + You can modify scripts for similar future analyses
  + When you receive an error from your code (and you will), you won't have to rewrite everything in the console
      + Edit the code without having to rewrite it

???

+ 

---
#  The tidyverse

+ Many ways to approach statistics with R


+ We're going to use the tidyverse
  + It's a **.blue[collection]** of packages that work with each other to make data analysis/graphing easy

.center[<img src="assets/img/image30.png">]

???

+ Many way to do stats with R...base R...or add packages
+ R exists as a base package with a reasonable amount of functionality
+ However, the beauty of R is that it can be expanded by downloading packages that add specific functionality to the program
  + The tidyverse is a collection of these packages we will use this semester 
+ It's a favorite in the industry
+ It makes many complex approaches in base R much simpler
+ Tends to be more intuitive, especially if you're new to R/RStudio
+ Easier for others to read code
+ 
+ So R is like a new mobile phone: while it has a certain amount of features when you use it for the first time, it doesn't have everything
+ R packages are like the apps you can download onto your phone from Apple's App Store or Android's Google Play

---
#  The tidyverse vs. base R

<br>
Base R:

```{r, eval=FALSE}
chicago_crime_2018[, c("type_of_crime", "count")]
```

<br>
tidyverse:

```{r, eval=FALSE}
chicago_crime_2018 %>% 
  select(type_of_crime, count)
```

???

+ With base R, you need to know more about programming
+ Whereas the tidyverse is very user-friendly...especially for beginners/novices

---
#  The tidyverse vs. base R

<br>
Base R:

```{r, eval=FALSE}
leave_house(get_dressed(get_out_of_bed(wake_up(me, time = "8:00"), side = "correct"), pants = TRUE, shirt = TRUE), train = TRUE, car = FALSE)
```

<br>
tidyverse:

```{r, eval=FALSE}
me %>% 
  wake_up(time = "8:00") %>% 
  get_out_of_bed(side = "correct") %>% 
  get_dressed(pants = TRUE, shirt = TRUE) %>% 
  leave_house(train = TRUE, car = FALSE)
```

???

+ the tidyverse is also easier to read and think about

---
#  The tidy format

<br>
<br>
<br>
<br>
.center[<img src="assets/img/image31.png">]

???

+ The tidyverse is based on the tidy format philosophy
+ There are three interrelated rules which make a data set tidy:
  + Each variable must have its own column
  + Each observation must have its own row
  + Each value must have its own cell

---
##  Installing & loading the tidyverse

<br>
+ In the console, type the following:

```{r, eval=FALSE}
install.packages("tidyverse")
```

+ Files will automatically download from CRAN

--

<br>
+ To use the packages, you must load them

```{r, eval=FALSE}
library(tidyverse)
```

+ You must do this every time you open a new RStudio session
+ Common practice to load packages at beginning of script

???

+ Point out difference between using quotation marks and not

---
#  Troubleshooting

1. Make the problem as small as possible...try to isolate what's happening <br><br>
1. Review your code and compare it to the demonstration script <br><br>
1. Check out the "Tutorial" tab in the upper right pane...or the "Primers" section under the "Learn" section on RStudio Cloud <br><br>
1. Google the error message
  + Remove any terms specific to your problem (e.g., object/variable names)
  + Add "tidyverse" as a keyword in your search <br><br>
1. Ask me...or a friend/colleague who knows R

.right[<img src="assets/img/image32.jpeg" width=40%>]

???

+ Show your screen to show them how to access the "Tutorials" and Primers" sections
