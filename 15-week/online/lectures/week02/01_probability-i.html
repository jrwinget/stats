<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Basic Probability I</title>
    <meta charset="utf-8" />
    <meta name="author" content="PSYC 304, Winget" />
    <script src="libs/header-attrs-2.6/header-attrs.js"></script>
    <script src="libs/kePrint-0.0.1/kePrint.js"></script>
    <link href="libs/lightable-0.0.1/lightable.css" rel="stylesheet" />
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

# Basic Probability I
### PSYC 304, Winget

---




#  So what is probability?

+ **.blue[SET:]** A well defined collection of things
  + Usually objects or events, sometimes people


+ **.purple[ELEMENT:]** One of the "things"
  + Each thing in the set is an element


+ If **.blue["students in this class"]** is the definition of the set, then **.purple[each of you]** are an element


.pull-left[
&lt;br&gt;
+ Example: 10 horses in a horse race
  + .blue[Horses in the race] is the set
  + .purple[Each horse] is an element
]


.pull-right[.center[
&lt;img src="assets/img/image01.jpg" width=75%&gt;
]]

???

+ This week, we'll talk about probability
+ The concept of probability is essential to statistical inference...concepts like chance, uncertainty, and the conditions under which events might occur are all common topics we'll encounter throughout the course
+ Probability is best understood in terms of set theory
+ So to start, we need to cover some basic definitions and operations of set theory
+ A set is a well defined collection of things...this is a highly technical term we're using here..."things"
  + Usually these "things" are objects or events...but they can also be people as well
+ This idea is pretty similar to the everyday concept of a set...like a set of silverware
+ The most important part about the concept of a set is that there is some quality the things in the set share that defines them as members in the set
  + Everything that does not share this quality are nonmembers of the set
+ Everything that is a member of the set is an element...so think of an element of a set as any one of the set's members
+ For example, if "**students in this class**" is the definition of the set, then **each of you** are an element
+ As another example, we can think about horses in a horse race
  + Let's say there are 10 horses in the race
  + The set would be all 10 of the horses in the race
  + And each element would be a single horse

---
#  A set: Horses in a race

.center[
&lt;img src="assets/img/image02.png" height=400&gt;

The circle defines the set and each letter is an element. So "things" in the circle are horses in the race (as opposed to those that are not), and each letter represents an element (i.e., a horse)
]


???

+ Here's a visual way to think about sets and elements
+ Let's stick with the horse race example...where we have 10 horses in a race as the set
+ The circle here represents the set
  + Anything inside the circle is a member of the set...and anything outside of the circle is NOT a member of the set
  + In this case the "things" are horses
+ Within the circle we have 10 elements...10 horses
  + Each horse is an element
+ To keep track of all of the elements...we can assign letters to each individual element
  + So the letters represent the individual horses...which are all of the individual elements in this set

---
#  Subset 

.center[
&lt;br&gt;
&lt;img src="assets/img/image04.png" width=250&gt;

&lt;br&gt;
&lt;br&gt;
A is *a subset of* B because all elements of A are in B
]

`$$A \subseteq B$$`

???

+ Now, a very important concept of probability is the subset
+ If every element in set A is also an element of set B, then A is a subset of B
+ Since the phrase "is a subset of" is somewhat long, we tend to use this sideways horseshoe looking symbol ( `\(\subseteq\)`) to represent "is a subset of"
+ Whenever we are dealing with a set or sets, it's customary to represent it with a capital letter...like A and B here
+ So `\(A \subseteq B\)` symbolizes the idea that A is a subset of B
+ Right now, we're talking about sets and subsets at a pretty abstract level, but you're all intuitively familiar with these concepts
+ e.g., plants is a subset of all living things...animals is a different subset of all living things...human beings is a subset of animals
  + Can anyone else think of an example of a subset?
  + Great work!
+ So, it's not too new...we're just introducing some theory and symbols to describe and talk about ideas you're already familiar with

---
#  Subset example 

.center[
&lt;img src="assets/img/image02.png" height=400&gt;

&lt;br&gt;
The set "black horses" {G, J, &amp; I} is a subset of horses in the race
]

???

+ Here's another example of a subset using the Horse Race example we've been talking about
+ Recall that we have 10 race horses in the set, with each element/horse represented by a unique letter
+ You'll also notice here that there are 2 colors of horses in the set...brown and black
+ Because they're all in the race, we can draw a circle around them to make the entire set of race horses
+ But, because there are 2 different color horses in this set, we can also distinguish between a subset of race horses from the larger set
+ So, if we were to focus only on the black horses, for example, they would be a subset of the larger set
  + The set of black horses...G, J, and I...is a subset of all horses in the race 
+ Does anyone have any questions at this point?

---
#  Union of two sets

.center[
&lt;br&gt;
&lt;img src="assets/img/image05.png" width=300&gt;

&lt;br&gt;
&lt;br&gt;
All elements in A and all elements in B – *any* element in A .red[**or**] B
]

`$$A \cup B$$`

???

+ An important operation of sets is the union of 2 sets
+ Let's say we have 2 sets...A and B like we have on the slide here
+ The UNION of A and B is the set of elements that are in A, that are in B, or that are in both A and B
  + So, basically it's everything between the two sets...in the picture it would be all of the purple area among the sets...including the shared area
+ Unions are represented by this horseshoe looking symbol...so the union of A and B is written like this... `\(A \cup B\)`...and it is read "A union B"
+ Again, this isn't really a new idea...it's just represented more abstractly than most people are accustomed to
+ e.g., The union of all children and adults is people
  + Children could be represented by set A...and adults by set B
  + Regardless if they are in set A, set B, or in the middle portion...they are still people
+ The crucial word to remember about unions is OR
+ The benchmark for including any element...any "thing"...in the union of A and B is whether that element is a part of EITHER set A OR set B

---
#  Union example

.center[
&lt;br&gt;
&lt;img src="assets/img/image09.png"&gt;
]

`$$A = \{1, 2, 3, 4, 5\}$$`

`$$B = \{3, 4, 5, 6, 7, 8\}$$`

`$$A \cup B = \{1, 2, 3, 4, 5, 6, 7, 8\}$$`

???

+ For example, if the set of A contained the values 1-5...and the set of B contained values 3-8...we'd see some values only appear in A (1, 2)
+ We'd also see some values only appear in B (6-8)
+ There would also be some values that appeared in each set (3-5)
+ But the UNION of A and B are all of the values that appear in A, all of the values that appear in B, and the values that appear in A or B

---
#  Intersection of two sets

.center[
&lt;br&gt;
&lt;img src="assets/img/image06.png" width=300&gt;

&lt;br&gt;
&lt;br&gt;
Elements that are in both A .red[**and**] B
]

`$$A \cap B$$`

???

+ Another operation on sets is the intersection of 2 sets
+ Keeping with the 2 sets A and B like we've been dealing with...
+ The INTERSECTION of A and B contains all elements that are in BOTH A and B...but NOT in A or B exclusively
  + So, basically it's everything in the overlapping area of the two sets...in the picture it's the purple area here
+ The symbol for intersections is this upside down looking horseshoe symbol...and the intersection of A and B is written `\(A \cap B\)`...read "A intersection B"
+ Again, nothing strikingly new here...just more abstract ways of representing familiar ideas
+ e.g., the intersection of all people younger than 22 and all people older than 18 is all people between 18 and 22 years of age
  + The intersection between people with red hair and people who wear glasses is people with red hair who wear glasses
  + Can anyone else think of an intersection of 2 sets?
  + Great example!
+ The crucial word to remember about intersections is AND
+ This is because for an element to be a member of the intersection A and B, it must be contained in BOTH A and B
+ The intersection represents the elements that are only shared by each of the 2 sets

---
#  Intersection example

.center[
&lt;br&gt;
&lt;img src="assets/img/image09.png"&gt;
]

`$$A = \{1, 2, 3, 4, 5\}$$`

`$$B = \{3, 4, 5, 6, 7, 8\}$$`

`$$A \cap B = \{3, 4, 5\}$$`

???

+ Going back to the number example we talked about for unions...
+ Let's assume the set of A contains the values 1-5...and the set of B contains values 3-8...we'd see some values only appear in A (1, 2)
+ We'd also see some values only appear in B (6-8)
+ There would also be some values that appeared in each set (3-5)
+ These values that appear in each set (3-5) are the ones INTERSECTIONS are concerned with
+ So the INTERSECTION of A and B is the set 3, 4, 5

---
#  Universal set

&lt;br&gt;
.center[&lt;img src="assets/img/image03.png" width=600&gt;]

`$$A \cup A' = S$$`

???

+ Now, there is a special type of set we need to talk about...the universal set
+ The universal set includes all things to be considered in any one discussion...and we represent it using the symbol S
+ In other words, the universal set is an inclusive set that defines the general type of objects being discussed
  + In this picture, think of the universal set as being BOTH the circle (set A) and the larger tan rectangle
  + Anything that is NOT in the set, is represented by A'...in set theory, this is read as "Not A"
  + So the universal set is everything that is part of set A as well as everything that is NOT A
  + This means that the UNION of A and A' is S (the universal set)
+ In other words, if S is the entire space...and A is a set in S...then A' represents the set of all elements in S that are not in set A
  + For this reason, A' is called the **complement** of A
+ Another way to think about this is in terms of populations and samples
  + The universal set is analogous to the population...whereas a set would be analogous to a sample
+ For example, let's say the population we're interested in is college students
  + In this case, the universal set (represented by S) would be all college students
  + Now, recall that a sample is just a subset of the population
  + So here, we might sample college students from various universities across the country to participate in a survey
  + The ones that participate would be part of the set A...so they would be in the circle
  + College students who DO NOT participate would be outside of the circle, but still in the tan rectangle...so they would be part of the set A'
  + Make sense?
+ Now, if we took the UNION of both A and A'...we would have the universal set...in this case ALL college students

---
#  Example: Union and intersection 

.center[A = set of vowels, B = brown letters]

.pull-left[&lt;br&gt;&lt;img src="assets/img/image10.png"&gt;]

.pull-right[
&lt;br&gt;
`$$C = A \cup B = \{A, B, C, D, E, F, H, I\}$$`

&lt;br&gt;
`$$C' = \{G, J\}$$`

&lt;br&gt;
`$$D = A \cap B = \{A, E\}$$`

&lt;br&gt;
`$$D' = \{B, C, D, F, G, H, I, J\}$$`
]

???

+ Now that we've covered the basics of set theory, let's practice a bit with the distinction between unions and intersections
+ Walk through the slide

---
#  Additional concepts 

+ **.blue[Empty set:]** No elements
  + Represented as `\(\varnothing\)`


+ **.blue[Disjoint sets:]** Sets with no common elements
  + Intersection is empty
  + `\(A \cap B = \varnothing\)`


.center[&lt;img src="assets/img/image11.png" height=175&gt;]


+ **.blue[Equal sets:]** Every element in `\(A\)` is in `\(B\)`, and every element in `\(B\)` is also in `\(A\)`
  + If `\(A \subseteq B\)` and `\(B \subseteq A\)`, then `\(A = B\)`
  + If `\(A = \{1, 2, 5, 9\}\)` and `\(B = \{5, 1, 9, 2\}\)`, then `\(A = B\)`

???

+ There are some other special cases of sets we should make note of
+ The first is the empty set
  + An empty set...aka a null set...contains NO elements...and it's symbolized by `\(\varnothing\)`
  + It's exactly what is sounds like...it's a set with nothing in it
+ Along similar lines, there is a special case of intersection where the intersection is empty...called a disjoint set
  + A disjoint set is when there are 2 sets...such as A and B...that do not have any common elements
  + No element of A is in set B...and no element of B is in set A...like in the image you see on this slide
  + Unlike an empty set, either set A or B can have elements in them in a disjoint set...it's the intersection of them that cannot contain any elements
+ The last case I want to talk about are equal sets
  + These are sets where every element of set A is also an element of set B AND every element of set B is also an element of set A
  + For example, if the set A was comprised of the values 1, 2, 5, 9 and the set B was comprised of the values 5, 1, 9, 2, then A and B would be equal sets
  + The order of the elements in the sets do not matter
  + If we added 3 to set A, sets A and B would no longer be equal...set B would instead be a subset of set A

---
#  Probability 

+ **.blue[Sample space:]** The set of things we are interested in
  + Universal set of a particular situation or experiment


+ **.blue[Elementary event or outcome:]** Each element in the set
  + Each thing that can happen or each thing that we can observe

&lt;br&gt;
&lt;br&gt;
.center[&lt;img src="assets/img/image12.jpg"&gt;]

???

+ As you'll soon see, set theory is a major component of probability
+ However, when dealing with probability questions, set theory concepts are given special names
  + Of course, right?....Mathematicians can't make it too easy
+ The collection of all the elements in a situation or experiment is called the sample space
  + The sample space corresponds to the universal set and is symbolized by S
+ The elements of this set are known as elementary events or outcomes
  + An event is any set of elements in the universal space...so think of it like the sets we used in previous examples
  + An elementary event may be an single event, a set of events, or all of the elementary events in the sample space
+ For example, if we were to roll a single die, the sample space is the set of ALL 6 elementary events (outcomes)...{1,2,3,4,5,6}
+ An example of an event would be rolling a 2
+ Another example of an event could be rolling a 3 or less...so a set comprised of rolling 1, 2, or 3
+ Or rolling an even number...a set comprised of 2, 4, or 6

---
#  Probability 

+ Sample space = `\(S\)` (universal set)


+ Event type of interest = `\(A\)` (subset of `\(S\)`)


+ Then, the probability of `\(A\)` occurring (or the probability of choosing a element in `\(A\)` in a sample from `\(S\)`) is defined by

&lt;br&gt;
`$$P(A) = \frac{\# (A)}{\# (S)}$$`

&lt;br&gt;
`$$P(head) = \frac{\#(head)}{\#(possible \space outcomes)} = \frac{1}{2}$$`

???

+ The sample space and the event type are what we're primarily concerned with when dealing with probability problems
+ Remember, the sample space is represented by S...it's the universal set we were talking about in set theory
  + The event type of interest is basically just a set...so we can represent that like we were with set A in our set theory examples
  + More specifically, the event type of interest is a **subset** of the universal set
  + Maybe flip back to slide 10 to show this idea
+ Now, in a sample space that has equally likely events...the probability of an event A is the number of outcomes in A...divided by the total number of outcomes in the sample space, S
+ So, if `\(\#(A)\)` represents the number of outcomes in A...and `\(\#(S)\)` represents the number of outcomes in the sample space...the probability of event A is shown in this equation
+ For example, let's say we're interested in the probability of getting heads when flipping a coin
  + Well, the sample space consists of the total number of outcomes...which is either heads or tails...so we get a 2 for the denominator
  + Set A consists of 1 outcome...getting heads...so we get a 1 for the numerator
  + We divide the number of outcomes in set A...1...by the number of outcomes in the sample space...2...and get the probability of getting heads... .5
+ One thing to note about probabilities is that they only apply **before** the actual event occurs
  + Once we flip the coin...for example...we end up with heads or we won't...the probability of .5 no longer applies

---
#  Examples 

.center[&lt;img src="assets/img/image02.png" height=300&gt;]


+ P(winning horse is black) = 3/10 = .30


+ P(brown horse winning) = 7/10 = .70


+ P(either a brown or a black horse winning) = 10/10 = 1.0

???

+ Let's go back to our horse racing example
+ Now, assuming all of the horses run approximately the same speed...
+ The probability of a black horse winning would be...
  + Explain
+ The probability of a brown horse winning would be...
  + Explain
+ The probability of EITHER a brown or a black horse winning would be...
  + Explain
+ What questions do you have at this point?

---
#  Class as sample space 

.pull-left[
+ Probability a randomly sampled student is... &lt;br&gt;&lt;br&gt;
  + A man
      + `\(7/36 = .194\)`
  + A woman
      + `\(29/36 = .806\)`
  + Age `\(\le\)` 19
      + `\(30/36 = .833\)`
  + A man and `\(\ge\)` 21
      + `\(1/36 = .028\)`
  + A woman and `\(\ge\)` 21
      + `\(2/36 = .056\)`
  + A woman **or** `\(\le\)` 19
      + `\(34/36 = .944\)`
]

.pull-right[
&lt;br&gt;
&lt;br&gt;
&lt;br&gt;
&lt;table class="table" style="font-size: 20px; margin-left: auto; margin-right: auto;"&gt;
 &lt;thead&gt;
  &lt;tr&gt;
   &lt;th style="text-align:left;"&gt; Age &lt;/th&gt;
   &lt;th style="text-align:right;"&gt; # of women &lt;/th&gt;
   &lt;th style="text-align:right;"&gt; # of men &lt;/th&gt;
   &lt;th style="text-align:right;"&gt; Total &lt;/th&gt;
  &lt;/tr&gt;
 &lt;/thead&gt;
&lt;tbody&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; &amp;lt;19 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 17 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 2 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 19 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; 19 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 8 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 3 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 11 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; 20 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 2 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 1 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 3 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; 21 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 1 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 1 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 2 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; &amp;gt;21 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 1 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 0 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 1 &lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
   &lt;td style="text-align:left;"&gt; Total &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 29 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 7 &lt;/td&gt;
   &lt;td style="text-align:right;"&gt; 36 &lt;/td&gt;
  &lt;/tr&gt;
&lt;/tbody&gt;
&lt;/table&gt;
]

???

+ Okay, let's make things a little more interesting
+ Let's look at some of the data from the survey you filled out last week
+ Walk through logic
  + For last one: have to add up total # of women (29) and total # of `\(\le\)` 19 (30)....then subtract women who are `\(\le\)` 19 (25)...to get total number of elements (34)
  + We'll talk more about this specific logic next class

---
background-image: url('assets/img/image07.jpeg')
background-size: contain

???

+ Now, for a real-life application...everything we've been talking about so far is perfectly suited for a lot of casino games...like roulette
+ Roulette is a simple game of chance that's popular around the world
+ The rules are quite straightforward
  + There is a wheel that has both black and red slots with the numbers 1-36...there's also a 0...and on some wheels 00 (like we have on our wheel here)
  + There is also a table that has all of the numbers on the wheel...and a few other selectors...and these are where you would place bets
  + After all of the players place their bets on the table...the dealer spins the wheel and throws a small ball down the chute in the middle of it
  + Whichever number on the wheel the ball lands on is the winning number
  + If you have a bet on that number/selector, then you'll get a payout
+ Looking at the table, you'll notice the main part consists of numbers 0 through 36...including 00
  + The rest are sectors for betting on groups of numbers...odd/even...red/black...1-18...19-36...1st dozen numbers (1-12)...and columns of numbers
+ In roulette, there are 2 main groups of bets...inside bets and outside bets
  + Inside bets...are bets that are placed on the numbers themselves
  + Outside bets...on the other hand...are the ones placed on selectors....like red or black...even/odd...etc
+ So in roulette...our sample space are all of the possible outcomes on the roulette wheel...in this case we have 38 possible outcomes on the wheel...36 numbers plus 0 and 00
  + The event type of interest...or the set we're interested in...is wherever we place our bet
+ Does any one have any questions here?

---
#  Roulette payoff probabilities 

.pull-left[.center[&lt;img src="assets/img/image08.png" width=70%&gt;]]

.pull-right[
+ `\(A = 1/38 = .0263\)`
+ `\(B = 2/38 = .0526\)`
+ `\(C = 3/38 = .0789\)`
+ `\(D = 4/38 = .1053\)`
+ `\(E = 5/38 = .1316\)`
+ `\(F = 6/38 = .1579\)`
+ `\(G = 12/38 = .3158\)`
+ `\(H = 12/38 = .3158\)`
+ `\(I = 18/38 = .4737\)`
+ `\(J = 18/38 = .4737\)`
+ `\(K = 18/38 = .4737\)`
]

???

+ Now, in roulette, where you place your chips matters
+ If we were to make an inside bet on number 30...set A on this picture...we would be betting on the ball landing on number 30 only
  + So the probability of this occurring would be 1 / 38, which is .0263
+ However, if we wanted to bet on 2 numbers, we could place our chips where the letter B is on this picture
  + Explain
+ C represents a bet of 3 numbers...in this case 19-21
  + Explain
+ D is a bet of 4 numbers...25,26,28,29
  + Explain
+ E bets on 0, 00, 1, 2, and 3
  + Explain
+ F bets on 4-9
  + Explain
+ G bets on everything in the middle column
  + Explain
+ H bets on the third dozen numbers...25-36
  + Explain
+ I bets on all red numbers
  + Explain
+ J bets on all odd numbers
  + Explain
+ K bets on the second half of numbers...19-36 
  + Explain
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
