library(car)
library(pwr2)
library(psych)
library(effectsize)
library(tidyverse)

# load and inspect data
plastics <- read_csv("01_plastic-pollution.csv")

glimpse(plastics)
describe(plastics)

# Between-groups t-test
# a priori power analysis
power.t.test(power = .8, type = "two.sample", alternative = "two.sided", delta = 1)

# Within-groups t-test

# One-way ANOVA

# Two-way ANOVA
